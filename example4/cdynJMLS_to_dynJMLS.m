function dynJMLS = cdynJMLS_to_dynJMLS(cdynJMLS)

nz = cdynJMLS.N;
dynJMLS.N = nz;
dynJMLS.lnT = cdynJMLS.lnT;
for m = 1:nz
    
    dynJMLS.model(m) = cdynLG_to_dynLG(cdynJMLS.model(m));
end
end


