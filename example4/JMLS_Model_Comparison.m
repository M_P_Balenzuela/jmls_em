function JMLS_Model_Comparison(JMLSactual,JMLS)

nz = JMLS.N;
Am = cell2mat({JMLS.model(:).A});
Ama = cell2mat({JMLSactual.model(:).A});


% Note that this approach will probably fall apart when A values are too
% similar
Mc = abs(Am.'-Ama);
IDXc = perms(1:nz); % map j=IDXc(i), where i=actual idx, j = JMLS idx
nIDXc = size(IDXc,1);

Tc = nan(1,nIDXc);
for i = 1:nIDXc
    Tc(i) = sum(Mc(IDXc(i,:)+(0:nz-1)*nz));
end
[~, mini] = min(Tc);
map = IDXc(mini,:);

% Actually do the mapping
for i=1:nz
    JMLSstateTrans.model(i) = SS_trans(JMLS.model(map(i)),JMLSactual.model(i));
end

JMLSstateTrans.T = nan(nz,nz);

for i=1:nz
    for j=1:nz
        JMLSstateTrans.lnT(i,j) = JMLS.lnT(map(i),map(j));
    end
end


for i = 1:nz
    disp('----------------------------------------------')
    disp(['Model ' num2str(i) ' results:'])
    disp(['True A(' num2str(i) '): ' num2str(JMLSactual.model(i).A) ', Found A(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).A)]);
    disp(['True B(' num2str(i) '): ' num2str(JMLSactual.model(i).B) ', Found B(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).B)]);
    disp(['True C(' num2str(i) '): ' num2str(JMLSactual.model(i).C) ', Found C(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).C) ' (EXACT from state transformation)']);
    disp(['True D(' num2str(i) '): ' num2str(JMLSactual.model(i).D) ', Found D(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).D)]);    
    
    disp(['True Q(' num2str(i) '): ' num2str(JMLSactual.model(i).Q) ', Found Q(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).Q)]);    
    disp(['True R(' num2str(i) '): ' num2str(JMLSactual.model(i).R) ', Found R(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).R)]);  
    if isfield(JMLSstateTrans.model(i),'S')
        disp(['True S(' num2str(i) '): ' num2str(JMLSactual.model(i).S) ', Found S(' num2str(i) '): ' num2str(JMLSstateTrans.model(i).S)]);   
    end
end

disp('----------------------------------------------')
for j=1:nz
    for i = 1:nz-1
        disp(['True T(' num2str(i) ',' num2str(j) '): ' num2str(exp(JMLSactual.lnT(i,j))) ', Found T(' num2str(i) ',' num2str(j) '): ' num2str(exp(JMLSstateTrans.lnT(i,j)))]);
    end
end


% For determining how active models were
% Tss = 0
% for k=1:N-1
% idx = JMLSs.den(k).z(1,:) == map(didx(k));
% Tss = Tss + sum(exp(JMLSs.den(k).lnw(idx)));
% end
% 
% Tss =Tss/(N-1)


