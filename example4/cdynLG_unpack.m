function [A,b,cQ,C,d,cR] = cdynLG_unpack(SS,ym,u)

persistent struct_UT 
if isempty(struct_UT) 
    struct_UT = struct('UT',true);
end


% Unpack a Linear Gaussian SS structure. This function also transforms the system to remove
% cross-covariance noise S.

% IN:
    % SS.A - A in x_{k+1} = Ax_k + Bu
    % SS.C - C in y = Cx + Du
    % SS.Q - Process noise covariance matrix
    % SS.R - Measurement noise covariance matrix
    
% OPTIONAL IN:
    % SS.B - B in x_{k+1} = Ax_k + Bu
    % SS.D - D in y = Cx + Du
    % SS.S - Cross-covaraince between process and measurement noise
    % ym - Measurement vector (required if SS.S exists)
    % u - Input vector
    
% OUT:
    % A - A in x_{k+1} = Ax_k + b
    % b - State prediction offset
    % Q - New process noise covariance matrix
    % C - C in y= Cx + d
    % d - Measurement prediction offset
    % R - Measurement noise covariance matrix
    


if nargin < 3
    u = zeros(0,1);
end
    
% Unpack required variables
A = SS.A;
C = SS.C;
cPi = SS.cPi;

% Grab dims.
[nu,~] = size(u);
[ny,nx] = size(C);

if isfield(SS,'B')
    B = SS.B;
else
    B = zeros(nx,nu);
end

if isfield(SS,'D')
    D = SS.D;
else
    D = zeros(ny,nu);
end

cR = cPi(1:ny,1:ny);
cQ = cPi(ny+1:end,ny+1:end);
H = cPi(1:ny,ny+1:end);
Gamma = linsolve(cR,H,struct_UT);
Gamma = Gamma.';
    
A = A-Gamma*C;
b = (B-Gamma*D)*u + Gamma*ym;
% C is as is
d = D*u;