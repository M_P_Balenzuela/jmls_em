function SStrans = SS_trans(SSEM,SSactual)
% This v1.0 of this function is for a single state system only (C must be invertable).

% Calculate Lambda
Lambda = SSactual.C\SSEM.C;

SStrans.A = Lambda*SSEM.A/Lambda; % The Lambda and invLambda cancels, but I'm leaving this in for future expansion reasons.
SStrans.B = Lambda*SSEM.B;
SStrans.C = SSEM.C/Lambda;
SStrans.D = SSEM.D;

SStrans.Q = Lambda*SSEM.Q*Lambda.';
SStrans.R = SSEM.R;
if isfield(SSEM,'S')
    SStrans.S = Lambda*SSEM.S;
end


end
