%% Clear workspace
clear all;
close all;
clc;

rng('shuffle');
rngseed = round(rand*10^4)
 rngseed = 24
rng(rngseed);

N = 2000;
x0 = zeros(5,1);
Niter = 2000000;

Likelihood_threshold = 0.03;
num_likelihood_cnd = 10;
bode_every = 100; % about 1hr 6 mins on my machine

%% Define True JMLS system        
% Model 1          
dynJMLS.model(1).A = [0.2064    -0.3052     0.2823     0.1248     0.2108;
      0.07292    0.09937    -0.3013     0.1846     0.3506;
        0.4733     0.1529     0.0824     0.0173   -0.07446;
    -0.001208     0.1613     0.1568     0.4747   -0.02424;
      -0.05965     0.3325     0.2412   -0.03493     0.3104];

dynJMLS.model(1).B = [     1.278;
      0;
      1.695;
      0.2386;
      0];
dynJMLS.model(1).C = [-1.02   0.6205  -0.4478  0.02722    0.504];
dynJMLS.model(1).D = 0;
dynJMLS.model(1).Q = diag([0.1 0.02 0.4 0.003 0.22]);
dynJMLS.model(1).R = 0.002;
dynJMLS.model(1).S = zeros(5,1);

%  Model 2
dynJMLS.model(2).A = [-0.02556    0.06122     0.2064     0.1604    0.01432;
      -0.02651     0.1085     -0.154     0.2721   -0.02609;
        0.1269    -0.1719     0.0814     0.1131    -0.1117;
        0.2304     0.2226    0.05397     0.2411    0.08365;
      -0.04902     0.1276    0.04064  -0.003305    -0.1814];
   
dynJMLS.model(2).B = [-0.03581;
     -0.9225;
     -0.00104;
      -0.5166;
        1.254];
dynJMLS.model(2).C = [0.3809  0.4529  -1.042  0.2038   1.822];
dynJMLS.model(2).D = 0;
dynJMLS.model(2).Q = diag([0.034 0.155 0.034 0.01 0.15]);
dynJMLS.model(2).R = 0.005;
dynJMLS.model(2).S = zeros(5,1);

nz = length(dynJMLS.model);
dynJMLS.N = nz;

dynJMLS.lnT = log([0.7 0.35;
              0.3 0.65]);


%% Simulate plant
u_arr = randn(1,N);
zini = round((nz-1)*rand(1)+1); % starting model
[xtrue_arr, ztrue_arr, y_arr] = dynJMLS_simulate(x0,dynJMLS,u_arr,zini);


%% Save true system
dynJMLS_actual = dynJMLS;

%% Inital EM Guess

dynJMLS.model(1).A = [-0.1308    0.1751   0.01805  -0.05234   -0.2421;
       0.1747   -0.3155    -0.284   0.03785    0.1922;
      0.01802   -0.2836   -0.1986    0.2049    0.2149;
     -0.05269   0.03761    0.2046    -0.127    -0.189;
      -0.2423    0.1924    0.2147   -0.1888   -0.2883];
dynJMLS.model(1).B = [0.1639;  -0.8637; -0.7496;  0; -1.228];
dynJMLS.model(1).C = [1.31   -1.455  -0.6922        0  -0.2645];
dynJMLS.model(1).D = 0.1;
%
dynJMLS.model(1).Q = dynJMLS.model(1).Q*5;
dynJMLS.model(1).R = dynJMLS.model(1).R*10;
dynJMLS.model(1).S = dynJMLS.model(1).S*10;



dynJMLS.model(2).A = [0.1705      0.1688     -0.1479      0.1096      -0.129;
         0.1659     0.09639   0.0005635      0.2211     -0.1187;
        -0.1422   -0.005439    -0.04148      -0.101  -0.0008951;
         0.1022      0.2245    -0.09213     0.08936    -0.09021;
        -0.1444     -0.1077    0.009306    -0.07967      0.1381];
dynJMLS.model(2).B = [0;     -1.437;      1.336;          0;     0.4993];
dynJMLS.model(2).C = [0.5234       0  -1.023       0       0];
dynJMLS.model(2).D = -0.5;
%
dynJMLS.model(2).Q = dynJMLS.model(2).Q*5;
dynJMLS.model(2).R = dynJMLS.model(2).R*10;
dynJMLS.model(2).S = dynJMLS.model(2).S*10;

% This initialisation will take 5-ever
% dynJMLS.model(2) = dynJMLS.model(1);
% % JMLS.model(2).A = JMLS.model(1).A*1.02
% % JMLS.model(2).A = JMLS.model(1).A*0.5;
% % JMLS.model(2).B = JMLS.model(1).B*1.5;
% % JMLS.model(2).C = JMLS.model(1).C*2;
% % JMLS.model(2).D = JMLS.model(1).D*3;
%  dynJMLS.model(2).Q = dynJMLS.model(1).Q*0.9999;
% JMLS.model(2).R = JMLS.model(1).R*10;

dynJMLS.lnT = log(1/dynJMLS.N*ones(dynJMLS.N));

%% Estimator prior

mu0 = zeros(5,1);
P0 = 3*eye(5);
lnw0 = log(1);
[~,z,lnw,mu,cP] = GM_to_cHGM(nz,lnw0,mu0,P0);
prior.z = z;
prior.lnw = lnw;
prior.mu = mu;
prior.cP = cP;

%% Estimator params
KLR_Mu = 6;
KLR_epsilon = 10^-10;

%% Convert the system to a cdynJMLS system
cdynJMLS = dynJMLS_to_cdynJMLS(dynJMLS);


%% Stard doing EM
lnL = nan;
no_est_lnT = true;
num_likelihood_cnd_met = 0;
LnL_arr = nan(Niter,1);
for itt = 1:Niter
    itt

    % save old
    cdynJMLS_old = cdynJMLS;
    dynJMLS_old = dynJMLS;
    old_lnL = lnL;
    
    
    archive.arch_dynJMLS(itt) = dynJMLS;
    
    tic
    [cdynJMLS,prior,lnL]  = cdynJMLS_EM(prior,u_arr,y_arr,cdynJMLS, KLR_Mu, KLR_epsilon);
    toc
	
	LnL_arr(itt) = lnL;
    
    disp(['Log-likelihood Increase: ' num2str(abs(old_lnL - lnL))]);
    
    if no_est_lnT
            if (itt > 1) && (abs(old_lnL - lnL) < Likelihood_threshold)
                num_likelihood_cnd_met = num_likelihood_cnd_met + 1;
                if num_likelihood_cnd_met == num_likelihood_cnd
                    no_est_lnT = false;
                    disp(['Estimation of transition probabilities commenced at iteration: ' num2str(itt)]);
                    T_est_it = itt;
                end
                
            else
                num_likelihood_cnd_met = 0;
                cdynJMLS.lnT = cdynJMLS_old.lnT;
            end
         
    end
    

    dynJMLS = cdynJMLS_to_dynJMLS(cdynJMLS);
    
    if mod(itt-1,bode_every)==0
        sys1=ss(dynJMLS.model(1).A,dynJMLS.model(1).B,dynJMLS.model(1).C,dynJMLS.model(1).D,1);
        sys2=ss(dynJMLS.model(2).A,dynJMLS.model(2).B,dynJMLS.model(2).C,dynJMLS.model(2).D,1);
        Tsys1=ss(dynJMLS_actual.model(1).A,dynJMLS_actual.model(1).B,dynJMLS_actual.model(1).C,dynJMLS_actual.model(1).D,1);
        Tsys2=ss(dynJMLS_actual.model(2).A,dynJMLS_actual.model(2).B,dynJMLS_actual.model(2).C,dynJMLS_actual.model(2).D,1);

        figure(1);clf;
        bode(sys1,'-b');
        hold on;
        bode(sys2,'-b');
        bode(Tsys1,'-r');
        bode(Tsys2,'-r');
        drawnow;
    end

%     colours = {'-k', '-k','-b','-r', '-k','--r','-b'}; % true colours, est colours, true off diag lnT terms, est off diag lnT terms, logL colour
%     dynJMLS_EM_plot(itt,dynJMLS_actual,dynJMLS,dynJMLS_old,old_lnL,lnL, colours);
    

end



% JMLS_Model_Comparison(dynJMLS_actual,dynJMLS);

%% True TF's

        Tsys1=ss(dynJMLS_actual.model(1).A,dynJMLS_actual.model(1).B,dynJMLS_actual.model(1).C,dynJMLS_actual.model(1).D,1);
        TF_sys1 = tf(Tsys1)
        Tsys2=ss(dynJMLS_actual.model(2).A,dynJMLS_actual.model(2).B,dynJMLS_actual.model(2).C,dynJMLS_actual.model(2).D,1);
        TF_sys2 = tf(Tsys2)
%% Bode plots


timestep = 1;%0.001;
dw = 0.01; w = 0.01:dw:pi;

A_arr = cat(4,cat(3,dynJMLS_actual.model(1).A,dynJMLS_actual.model(2).A),cat(3,dynJMLS_inital_guess.model(1).A,dynJMLS_inital_guess.model(2).A),cat(3,dynJMLS.model(1).A,dynJMLS.model(2).A));

B_arr = cat(4,cat(3,dynJMLS_actual.model(1).B,dynJMLS_actual.model(2).B),cat(3,dynJMLS_inital_guess.model(1).B,dynJMLS_inital_guess.model(2).B),cat(3,dynJMLS.model(1).B,dynJMLS.model(2).B));

C_arr = cat(4,cat(3,dynJMLS_actual.model(1).C,dynJMLS_actual.model(2).C),cat(3,dynJMLS_inital_guess.model(1).C,dynJMLS_inital_guess.model(2).C),cat(3,dynJMLS.model(1).C,dynJMLS.model(2).C));

D_arr = cat(4,cat(3,dynJMLS_actual.model(1).D,dynJMLS_actual.model(2).D),cat(3,dynJMLS_inital_guess.model(1).D,dynJMLS_inital_guess.model(2).D),cat(3,dynJMLS.model(1).D,dynJMLS.model(2).D));


nM = size(A_arr,4);
[bode_mag,bode_phase, map_arr] = generate_bodes(w,timestep, A_arr,B_arr,C_arr,D_arr);

map = map_arr(:,2);
T_found = nan(cdynJMLS.N,cdynJMLS.N);
for i=1:cdynJMLS.N
    for j=1:cdynJMLS.N
        T_found(i,j) = dynJMLS.lnT(map(i),map(j));
    end
end
T_found = exp(T_found)
T_true = exp(dynJMLS_actual.lnT)

figure(1);
clf;
a = 0.06;
[ha,~] = tight_subplot(2,1,[a/2 a]*2,[a*2 a],[a*2 a/2]);

for i=1:cdynJMLS.N  % mode
    for m = 1:nM % method

    
        

        a = bode_mag(:,i,m);
        b = bode_phase(:,i,m);
        
        num_rev_off = round((bode_phase(1,i,1) - b(1))/360);

        b = b + num_rev_off*360;

        axes(ha(1))
        semilogx_5(w,20*log10((a)),m);
        if (m==nM)
            legend('True','Initial Guess','Proposed EM');
        end
        title(['Bode Diagram']); % (Model ' num2str(i) ')
        ylabel('Magnitude (dB)');
        hold on
        xlim([w(1) w(end)]);
        set(gca,'fontsize', 18);

        axes(ha(2))
        semilogx_5(w,b,m);
        ylabel('Phase (deg)');
        xlabel('Frequency (rad/s)');
        hold on;
        xlim([w(1) w(end)]);
        
        set(gca,'fontsize', 18);
    end
    
end



