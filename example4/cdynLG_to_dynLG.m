function dynLG = cdynLG_to_dynLG(cdynLG)


    dynLG.A = cdynLG.A;
    
    if isfield(cdynLG,'B')
        dynLG.B = cdynLG.B;
    end
    
    C = cdynLG.C;
    dynLG.C = C;
    ny = size(C,1);
    
    if isfield(cdynLG,'D')
        dynLG.D = cdynLG.D;
    end
    
    Pi = cdynLG.cPi;
    Pi = Pi.'*Pi;
    
    dynLG.R = Pi(1:ny,1:ny);
    dynLG.Q = Pi(ny+1:end,ny+1:end);
    dynLG.S = Pi(ny+1:end,1:ny);
    
end