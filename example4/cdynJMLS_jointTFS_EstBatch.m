function [JMLSF, JMLSS, lnL] = cdynJMLS_jointTFS_EstBatch(cdynJMLS, u_arr,y_arr,prior,KLR_Mu, KLR_epsilon)

% Grab dimensions
[~,N] = size(u_arr);

% Set prior
zp = prior.z;
lnwp = prior.lnw;
mup = prior.mu;
cPp = prior.cP;

JMLSF.pdist(1).zp = zp;
JMLSF.pdist(1).lnwp = lnwp;
JMLSF.pdist(1).mup = mup;
JMLSF.pdist(1).cPp = cPp;
    

lnL = 0;
% Run the JMLS filter over the timeseries
for k = 1:N
    u = u_arr(:,k);
    ym = y_arr(:,k);

    % Run JMLS filter
    [lnwf,muf,cPf,zf,lnwp,mup,cPp,zp, lnLk] = cdynJMLS_filter(lnwp,mup,cPp,zp,cdynJMLS,ym,u);
    
    lnL = lnL + lnLk;
    
    % KL Reduction on prediction distribution
    [zp, lnwp, mup, cPp] = cHGM_KLR(zp,lnwp, mup, cPp, KLR_Mu);

    % Save Filtered distribution
    JMLSF.dist(k).z = zf;
    JMLSF.dist(k).lnw = lnwf;
    JMLSF.dist(k).mu = muf;
    JMLSF.dist(k).cP = cPf;
    
    % Save predicted distribution
    JMLSF.pdist(k+1).z = zp;
    JMLSF.pdist(k+1).lnw = lnwp;
    JMLSF.pdist(k+1).mu = mup;
    JMLSF.pdist(k+1).cP = cPp;
end


if (nargout > 1)
    % Suppose I should make the smoothed distribution then
    
        zf = JMLSF.dist(N-1).z;
        lnwf = JMLSF.dist(N-1).lnw;
        muf = JMLSF.dist(N-1).mu;
        cPf = JMLSF.dist(N-1).cP;
        
        u = u_arr(:,N);
        ym = y_arr(:,N);
        
        u_pr = u_arr(:,N-1);
        ym_pr = y_arr(:,N-1);
        
        % Call the joint TF smoother
        [cLc,sc,rc,zc,lnws,mus,cPs,zs] = cdynJMLS_jointTFS(cdynJMLS,u,ym,cPf,muf,lnwf,zf, ym_pr,u_pr);

        
        % Save the first joint smoothed distribution
        JMLSS.dist(N-1).z = zs;
        JMLSS.dist(N-1).lnw = lnws;
        JMLSS.dist(N-1).mu = mus;
        JMLSS.dist(N-1).cP = cPs;       
        
    % Run the joint TF smoother over the timeseries
    for k = N-2:-1:1

        u = u_pr;
        ym = ym_pr;
        u_pr = u_arr(:,k);
        ym_pr = y_arr(:,k);
        
        % Load the filtered distribution
        zf = JMLSF.dist(k).z;
        lnwf = JMLSF.dist(k).lnw;
        muf = JMLSF.dist(k).mu;
        cPf = JMLSF.dist(k).cP;
        
        % KL Likelihood reduction
        [zc,cLc, sc, rc] = cHGML_KLR(zc,cLc, sc, rc, KLR_epsilon, KLR_Mu);
        
        % Call the joint TF smoother
        [cLc,sc,rc,zc,lnws,mus,cPs,zs] = cdynJMLS_jointTFS(cdynJMLS,u,ym,cPf,muf,lnwf,zf, ym_pr,u_pr,cLc,sc,rc,zc);

        
        % Store the joint Smoothed distribution [xk;xk+1]
        JMLSS.dist(k).z = zs;
        JMLSS.dist(k).lnw = lnws;
        JMLSS.dist(k).mu = mus;
        JMLSS.dist(k).cP = cPs;

    end  
end