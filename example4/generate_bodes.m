function [bode_mag,bode_phase, map_arr] = generate_bodes(w,timestep, A_arr,B_arr,C_arr,D_arr)

% x_arr(:,:,i,1) is the i-th sub-model in the true (actual) model
[~,~,nz,nM] = size(A_arr); % nM is the number of methods

 map_arr = nan(nz,nM);
bode_mag = nan(length(w),nz,nM);
bode_phase = nan(length(w),nz,nM);
for m = 1:nM
    for i = 1:nz
        A = A_arr(:,:,i,m);B = B_arr(:,:,i,m);C = C_arr(:,:,i,m);D = D_arr(:,:,i,m);
        SSsysi = ss(A,B,C,D,timestep);
        [magi,phasei] = bode(SSsysi,w);
        bode_mag(:,i,m) = squeeze(magi);
        bode_phase(:,i,m) = squeeze(phasei);
    end
    if m >1
        map = find_map(nz,bode_mag(:,:,1),bode_mag(:,:,m)); % map for m-th method
        map_arr(:,m) = map.';
        bode_mag(:,:,m) = bode_mag(:,map,m);
        bode_phase(:,:,m) = bode_phase(:,map,m);
    end
end


end



function map = find_map(nz,true_params,est_params)
% given true_params(:,i) and est_params(:,j), find the j=map(i) minimising
% the norm2 of the permutations

    Mc = nan(nz,nz);
    for i = 1:nz
        for j = 1:nz
            Mc(i,j) = norm(true_params(:,j) - est_params(:,i));        
        end
    end

    IDXc = perms(1:nz); % map j=IDXc(c,i), where i=actual idx, j = JMLS idx, c=combination number
    nIDXc = size(IDXc,1);


    Tc = nan(1,nIDXc);
    for i = 1:nIDXc
        Tc(i) = sum(Mc(IDXc(i,:)+(0:nz-1)*nz));
    end
    [~, mini] = min(Tc);
    map = IDXc(mini,:);
end