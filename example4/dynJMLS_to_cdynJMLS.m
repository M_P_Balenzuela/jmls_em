function cdynJMLS = dynJMLS_to_cdynJMLS(dynJMLS)

nz = dynJMLS.N;
cdynJMLS.N = nz;
cdynJMLS.lnT = dynJMLS.lnT;
for m = 1:nz
    
    cdynJMLS.model(m) = dynLG_to_cdynLG(dynJMLS.model(m));
end
end
