function [cdynJMLS,prior,lnL]  = cdynJMLS_EM(prior,u_arr,y_arr,cdynJMLS, KLR_Mu, KLR_epsilon)
% Perform one itteration of the EM algorithm for a chol- dynamic JMLS


persistent struct_UT 
if isempty(struct_UT) 
    struct_UT = struct('UT',true);
end


% E-Step
 [~, JMLSS, lnL] = cdynJMLS_jointTFS_EstBatch(cdynJMLS, u_arr, y_arr, prior, KLR_Mu, KLR_epsilon);
 

%% M-Step

nz = cdynJMLS.N; 
nx = size(prior.mu,1);
nu = size(u_arr,1);
ny = size(y_arr,1);
N = length(JMLSS.dist);

two_nx = 2*nx;
nE = two_nx+ny+nu;
LargeExpectation = zeros(nE,nE,nz);

lnTk = -inf*ones(nz);

for k = 1:N

    M = size(JMLSS.dist(k).mu,2);
    ym = y_arr(:,k);
    u = u_arr(:,k);
        
    lnw_arr = JMLSS.dist(k).lnw;
    mu_arr = JMLSS.dist(k).mu;
    cP_arr = JMLSS.dist(k).cP;
    z_arr = JMLSS.dist(k).z;
        
    for l=1:M
        zl = z_arr(1,l);        
        
        lnwl = lnw_arr(l);
        mul = mu_arr(:,l);
        cPl = cP_arr(:,:,l);
        
        MEE = cEM_joint_expectation(cPl, mul, lnwl, u, ym);
        
        QR1 = [LargeExpectation(:,:,zl); MEE];
        tmp = triu(qr(QR1));
        LargeExpectation(:,:,zl) = tmp(1:nE,1:nE);

    end
    
    for i = 1:nz
        for j = 1:nz
            idxx = sum((z_arr==[j;i]),1)==2;
            idxx = idxx.';
            
            tmpa = [lnTk(i,j);lnw_arr(idxx)];
            lnTk(i,j) = LSE(tmpa);
        end
    end
									
end


cdynJMLS.lnT = lnw_normalise(lnTk); % protect against inpossible models here and in noramlise_lnw function

Cme = LSE(lnTk);
Scale = -0.5*Cme;
Scale = exp(Scale);


nx_plus_nu = nx + nu;

% (E[xk;xk+1;uk;yk][.]^T )^0.5 * TTrans = (E[xk;uk;yk;xk+1][.]^T )^0.5
TTrans = [ eye(nx) zeros(nx,nx+nu+ny);
           zeros(nx,nx+nu+ny) eye(nx);
           zeros(nu,nx) eye(nu) zeros(nu,nx+ny);
           zeros(ny,nx+nu) eye(ny) zeros(ny,nx)];
for m = 1:nz
    
        % if model was active ...
        if (Cme(m) ~= -inf)
            
            QR2 = LargeExpectation(:,:,m) * TTrans;
            tmp = triu(qr(QR2));
            
            RA = tmp(1:nx_plus_nu, 1:nx_plus_nu);
            RB = tmp(1:nx_plus_nu, nx_plus_nu+1:end);
            RC = tmp(nx_plus_nu+1:end,nx_plus_nu+1:end);
            
            Gamma = linsolve(RA,RB, struct_UT);
            Gamma = Gamma.';
            
            cdynJMLS.model(m).C = Gamma(1:ny,1:nx);
            cdynJMLS.model(m).D = Gamma(1:ny,nx+1:end);
            cdynJMLS.model(m).A = Gamma(ny+1:end,1:nx);
            cdynJMLS.model(m).B = Gamma(ny+1:end,nx+1:end);            
            
            cdynJMLS.model(m).cPi = Scale(m)*RC;

        end
end


% Generate new prior
prior.mu = JMLSS.dist(1).mu(1:nx,:);
prior.cP = JMLSS.dist(1).cP(1:nx,1:nx,:);
prior.z = JMLSS.dist(1).z(1,:).';
prior.lnw = JMLSS.dist(1).lnw;

[prior.z,prior.lnw, prior.mu, prior.cP] = cHGM_KLR(prior.z,prior.lnw, prior.mu, prior.cP, KLR_Mu);


