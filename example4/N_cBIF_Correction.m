function [cLc,sc,rc] = N_cBIF_Correction(cLb,sb,rb,C,d_minus_ym, cR)

persistent struct_UT_TRANSA  
if isempty(struct_UT_TRANSA) 
    struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   % is the argument UT, does it need to be transposed?
end

% Get dimensions
[ny,nx] = size(C);

%% Measurement Update

tmp = linsolve(cR,C,struct_UT_TRANSA);

QR1 = [cLb;tmp];

R1 = triu(qr(QR1));
cLc = R1(1:nx,1:nx);

cinvRo = linsolve(cR,d_minus_ym,struct_UT_TRANSA);

sc  = sb + tmp.'*cinvRo;

if nargout > 2
    rc = TrLn(cR);
    rc = 2*rc;
    rc = rc + ny*log(2*pi);
    rc = rc + cinvRo.'*cinvRo;
    rc = rc + rb;
end