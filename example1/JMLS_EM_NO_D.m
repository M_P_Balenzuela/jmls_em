function [JMLS,prior,lnL]  = JMLS_EM_NO_D(prior,u_arr,y_arr,JMLS,KLR_Mu, KLR_epsilon)
% Perform one itteration of the EM algorithm for a linear-Gaussian system


% E-Step
[~,JMLSS, lnL] = JMLS_jointTFS_EstBatch(JMLS, u_arr,y_arr,prior,KLR_Mu, KLR_epsilon);

nu = size(u_arr,1);
[ny,nx] = size(JMLS.model(1).C);

nz = JMLS.N;
N = length(JMLSS.den);

Exp1 = zeros(nx,nx+nu,nz);
Exp2 = zeros(nx+nu,nx+nu,nz);
Exp3 = zeros(nx,nx,nz);

Exp4 = zeros(ny,nx,nz);
Exp5 = zeros(nx,nx,nz);
Exp6 = zeros(ny,ny,nz);

Tnm = zeros(nz,nz);
Cm = zeros(nz,1);
Cmp1 = zeros(nz,1);

for k = 1:N
    M = size(JMLSS.den(k).mu,2);
    y = y_arr(:,k+1);
    u = u_arr(:,k+1);
    for l = 1:M
        % Joint [xk; xk+1]
       
        wl = exp(JMLSS.den(k).lnw(l));
        mul = JMLSS.den(k).mu(:,l);
        Pl = JMLSS.den(k).P(:,:,l);
        j = JMLSS.den(k).z(1,l);
        i = JMLSS.den(k).z(2,l);
        
        muk = mul(1:nx);
        Pk = Pl(1:nx,1:nx);
        mukp1 = mul(nx+1:end);
        Pkp1 = Pl(nx+1:end,nx+1:end);
        Mk = Pl(nx+1:end,1:nx);
        
        
        % Process expectations
        Exp1(:,:,i) = Exp1(:,:,i) + wl*[mukp1*muk.'+Mk,     mukp1*u.']; % E[xk+1 [xk; uk+1]^T]
        
        Exp2(:,:,i) = Exp2(:,:,i) + wl*[muk*muk.'+Pk,       muk*u.';    % E[xk;uk+1][xk;uk+1]^T
                                           u*muk.',         u*u.'];
                                       
        Exp3(:,:,i) = Exp3(:,:,i) + wl*(mukp1*mukp1.'+Pkp1);            % E[xk+1 xk+1^T]
        
        
        % Measurement expectations
        Exp4(:,:,i) = Exp4(:,:,i) + wl*[y*mukp1.'];   % E[yk+1][xk+1;uk+1]^T 
        
        Exp5(:,:,i) = Exp5(:,:,i) + wl*[mukp1*mukp1.'+Pkp1]; % E[xk+1;uk+1][xk+1;uk+1]^T
                                        
                                     
        Exp6(:,:,i) = Exp6(:,:,i) + wl*(y*y.');            % E[yk+1 yk+1^T] 
                                    
        % Transition expections                            
        Tnm(i,j) = Tnm(i,j) + wl;                               
        
        Cm(j) = Cm(j) + wl;
        Cmp1(i) = Cmp1(i) + wl;
    end
    
end


for m = 1:nz
    

        GammaP = Exp1(:,:,m)/Exp2(:,:,m);
        JMLS.model(m).A = GammaP(:,1:nx);
        JMLS.model(m).B = GammaP(:,nx+1:end);
        
        if (Cmp1(m) ~= 0 )
%             GammaP = [JMLS.model(m).A JMLS.model(m).B];
            JMLS.model(m).Q = (Exp3(:,:,m) - 2*GammaP*Exp1(:,:,m).' + GammaP*Exp2(:,:,m)*GammaP.')/Cmp1(m); % This expression works even if GammaP is not replaced with the optimal value
        end
        
        
        GammaM = Exp4(:,:,m)/Exp5(:,:,m);
        JMLS.model(m).C = GammaM;
        
        % NO D
        JMLS.model(m).D = zeros(ny,nu);
        if (Cmp1(m) ~= 0 )
%             GammaM = [JMLS.model(m).C JMLS.model(m).D];   
            JMLS.model(m).R = (Exp6(:,:,m) - 2*GammaM*Exp4(:,:,m).' + GammaM*Exp5(:,:,m)*GammaM.')/Cmp1(m); % This expression works even if GammaP is not replaced with the optimal value
        end
    if (Cm(m) ~= 0 )
        JMLS.lnT(:,m) = log( Tnm(:,m) / Cm(m));
    end
end


% Generate new prior
prior.mu = JMLSS.den(1).mu(1:nx,:);
prior.P = JMLSS.den(1).P(1:nx,1:nx,:);
prior.z = JMLSS.den(1).z(1,:).';
prior.lnw = JMLSS.den(1).lnw;

[prior.z,prior.lnw, prior.mu, prior.P] = HGM_KLR(prior.z,prior.lnw, prior.mu, prior.P, KLR_Mu);


