function [JMLSF, JMLSS, lnL] = JMLS_jointTFS_EstBatch(JMLS, u_arr,y_arr,prior,KLR_Mu, KLR_epsilon)

% Grab dimensions
[~,N] = size(u_arr);

% Set prior
zf = prior.z;
lnwf = prior.lnw;
muf = prior.mu;
Pf = prior.P;

JMLSF.den(1).z = zf;
JMLSF.den(1).lnw = lnwf;
JMLSF.den(1).mu = muf;
JMLSF.den(1).P = Pf;

lnL = 0;
% Run the JMLS filter over the timeseries
for k = 1:N-1
    u = u_arr(:,k+1);
    ym = y_arr(:,k+1);

    % Run JMLS filter
    [lnwf,muf,Pf,zf, lnLk] = JMLS_filter(lnwf,muf,Pf,zf,JMLS,ym,u);
    
    lnL = lnL + lnLk;
    
    % KL Reduction
    [zf, lnwf, muf, Pf] = HGM_KLR(zf,lnwf, muf, Pf, KLR_Mu);

    % Save Filtered density
    JMLSF.den(k+1).z = zf;
    JMLSF.den(k+1).lnw = lnwf;
    JMLSF.den(k+1).mu = muf;
    JMLSF.den(k+1).P = Pf;
end


if (nargout > 1)
    % Suppose I should make the smoothed distribution then
    
        zf = JMLSF.den(N-1).z;
        lnwf = JMLSF.den(N-1).lnw;
        muf = JMLSF.den(N-1).mu;
        Pf = JMLSF.den(N-1).P;
        
        u = u_arr(:,N);
        ym = y_arr(:,N);
        
        % Call the joint TF smoother
        [Lb,sb,rb,zb,lnws,mus,Ps,zs] = JMLS_jointTFS(JMLS,u,ym,Pf,muf,lnwf,zf);
        
        % Save the first joint smoothed distribution
        JMLSS.den(N-1).z = zs;
        JMLSS.den(N-1).lnw = lnws;
        JMLSS.den(N-1).mu = mus;
        JMLSS.den(N-1).P = Ps;       
        
    % Run the joint TF smoother over the timeseries
    for k = N-2:-1:1

        u = u_arr(:,k+1);
        ym = y_arr(:,k+1);
        
        % Load the filtered distribution
        zf = JMLSF.den(k).z;
        lnwf = JMLSF.den(k).lnw;
        muf = JMLSF.den(k).mu;
        Pf = JMLSF.den(k).P;
        
        % Call the joint TF smoother
        [Lb,sb,rb,zb,lnws,mus,Ps,zs] = JMLS_jointTFS(JMLS,u,ym,Pf,muf,lnwf,zf,Lb,sb,rb,zb);

        % KL Likelihood reduction
        [zb,Lb, sb, rb] = HGML_KLR(zb,Lb, sb, rb, KLR_epsilon, KLR_Mu);
        
        % Store the joint Smoothed distribution [xk;xk+1]
        JMLSS.den(k).z = zs;
        JMLSS.den(k).lnw = lnws;
        JMLSS.den(k).mu = mus;
        JMLSS.den(k).P = Ps;

    end  
end