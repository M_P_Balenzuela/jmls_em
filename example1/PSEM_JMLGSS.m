function [ res ] = PSEM_JMLGSS( sys, init, opt )
%PSEM_JMLGSS PSAEM for jump Markov linear (JMLGSS) models.
%    PSEM for identification for models on the form
%
%   s(t+1) ~ p(s(t+1)|s(t)) = PI(s(t),s(t+1))
%   z(t+1) = A(s(t+1))*z(t) + B(s(t+1))*u(t) + w(t)
%   y(t)   = C(s(t))zx(t)                    + e(t)
%
%   s(t) belongs to {1, ..., K} for all t, and
%   E[w(t)'*w(t)] = Q(s(t+1))
%   E[e(t)'*e(t)] = R(s(t+1))
%   E[w(t)'*e(t)] = 0.
% 
%   length(z(t)) = nz
%   length(u(t)) = nu
%   length(y(t)) = ny
% 
%   Struct sys must also contain measurements Y, length of Y as sys.T
%   and inputs U.
%
%   Struct init must contain initial guesses of A, B, C, Q, R, PI.
%
%   Struct opt must contain options for PSAEM, as opt.num_iter
%   (number of iterations), opt.N (number of particles) and opt.M (number
%   of sampled backwards trajectories)
%   
%   Reference: Thomas B. Sch�n, Adrian Wills and Brett Ninness. System identification
%   of nonlinear state-space models. Automatica, 47(1):39-49, January 2011. 


% Intitialization and pre-allocation
A = init.A; B = init.B; C = init.C; Q = init.Q; R = init.R; PI = init.PI;
P0 = sys.P0; z0 = sys.z0; N = opt.N; num_iter = opt.num_iter; M = opt.M;
K = sys.K; nz = sys.nz; nu = sys.nu; ny = sys.ny; T = sys.T; Y = sys.Y; U = sys.U;
Ahist = zeros(nz,nz,K,num_iter+1); Bhist = zeros(nz,nu,K,num_iter+1);
Chist = zeros(ny,nz,K,num_iter+1); Qhist = zeros(nz,nz,K,num_iter+1);
Rhist = zeros(ny,ny,K,num_iter+1); 
lnThist = zeros(K,K,num_iter+1); 
Ahist(:,:,:,1) = A; Bhist(:,:,:,1) = B;
Chist(:,:,:,1) = C; Qhist(:,:,:,1) = Q; Rhist(:,:,:,1) = R;
lnThist(:,:,1) = log(PI.');
LL = zeros(1,num_iter);

for k = 1:num_iter
%     tic;
    
    % Pre-allocate
    w = zeros(N,T); s_pf = zeros(T,N); z_pf = zeros(nz,T,N);
    
    % Initialize
    w(:,1) = ones(size(w(:,1))); w(:,1) = w(:,1)./sum(w(:,1));
    

    %%%%%%%%%%%%
    % RB-FFBSi %
    %%%%%%%%%%%%
    
    % Generate first sample
    [s_pf(1,:),~] = find(mnrnd(1,ones(N,K)./K)');
    z_pf(:,1,:) = mvnrnd(repmat(z0',N,1),P0)';
    
    for t = 1:T
        
        % PF time propagation and resampling
        if t >= 2
            [a,~] = find(mnrnd(1,repmat(w(:,t-1)',N,1))');
            [s_pf(t,:),~] = find(mnrnd(1,PI(s_pf(t-1,a),:))');
			for i = 1:N
				z_pf(:,t,i) = mvnrnd(A(:,:,s_pf(t,i))*z_pf(:,t-1,a(i)) + B(:,:,s_pf(t,i))*U(:,t-1),Q(:,:,s_pf(t,i)));
			end
        end
       
        % PF weight update        
        lw = zeros(N,1);
        for i = 1:N
            y_pfP = C(:,:,s_pf(t,i))*z_pf(:,t,i);
            lw(i) = -1/2*log(det(R(:,:,s_pf(t,i)))) - 1/2*((y_pfP-Y(:,t))'/R(:,:,s_pf(t,i)))*(y_pfP-Y(:,t));
        end
        w(:,t) = exp(lw-max(lw));
        w(:,t) = w(:,t)/sum(w(:,t));
    end
    
	% Particle smoothing
    % Pre-allocate
	z_pfP1cs = zeros(nz,N);
    wt = zeros(T,N,M);
    s_ps = zeros(T,M);
    z_ps = zeros(nz,T,M);
    
    % Initialize
    [b,~] = find(mnrnd(1,repmat(w(:,T)',M,1))');
    s_ps(T,:) = s_pf(T,b);
	z_ps(:,T,:) = z_pf(:,T,b);
    
    
    for t = T-1:-1:1
        for j = 1:M
			for i = 1:N
				z_pfP1cs(:,i) = A(:,:,s_ps(t+1,j))*z_pf(:,t,i) + B(:,:,s_ps(t+1,j))*U(:,t);
			end
			wt(t,:,j) = mvnpdf(z_pfP1cs',repmat(z_ps(:,t+1,j)',N,1),Q(:,:,s_ps(t+1,j))).*PI(s_pf(t,:),s_ps(t+1,j));
            wt(t,:,j)= wt(t,:,j)./sum(wt(t,:,j));
            [b,~] = find(mnrnd(1,wt(t,:,j)')');
            s_ps(t,j) = s_pf(t,b);
			z_ps(:,t,j) = z_pf(:,t,b);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Compute and update sufficient statistics %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    S1 = zeros(K,K); S2 = zeros(K,1);
	S3 = zeros(3*nz+nu+ny,3*nz+nu+ny,K);
    
    for n = 1:K
        % S1
        for m = 1:K
            for j = 1:M
                S1(n,m) = S1(n,m) + 1/M*numel(strfind(s_ps(:,j)',[n m]));
            end
            S1(n,m) = 1/M*S1(n,m);
        end
        
        % S2
        S2(n) = sum(sum(s_ps(2:end,:)==n)'/M);
        
        % S3
        for j = 1:M
            for t = 2:1:T
                xi_t_hat = [z_ps(:,t,j); z_ps(:,t-1,j); U(:,t-1); Y(:,t); z_ps(:,t,j)];
                S3(:,:,n) = S3(:,:,n) + 1/M*(s_ps(t,j)==n) * (xi_t_hat*xi_t_hat');
            end

        end
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute log likelihood %
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLT = 0;
    LLTopt = 0;
    LL(k) = sum(sum(S1.*log(PI)));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Estimate new parameters %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for n = 1:K
        if S2(n) > 0
        
            Phi     = S3(1:nz,1:nz,n);
            Psi     = S3(1:nz,nz+(1:nz+nu),n);
            Sigma   = S3(nz+(1:nz+nu),nz+(1:nz+nu),n);

            Xi      = S3(2*nz+ny+nu+(1:nz),2*nz+ny+nu+(1:nz),n);
            Lambda  = S3(2*nz+nu+(1:ny),2*nz+ny+nu+(1:nz),n);
            Omega   = S3(2*nz+nu+(1:ny),2*nz+nu+(1:ny),n);

            % Part of LL computation
            LLT = LLT - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
                trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
                trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));

            PI(n,:) = S1(n,:)./sum(S1(n,:));

            AB = Psi/Sigma;
            CD = Lambda/Xi;

            A(:,:,n) = AB(:,1:nz);                       
            B(:,:,n) = AB(:,nz+1:end);                   
            C(:,:,n) = CD(:,1:nz);                       
            Q(:,:,n) = (Phi - (Psi/Sigma)*Psi')/S2(n);      
            R(:,:,n) = (Omega - (Lambda/Xi)*Lambda')/S2(n); 
            
            Q(:,:,n) = 1/2*(Q(:,:,n) + Q(:,:,n)');
            R(:,:,n) = 1/2*(R(:,:,n) + R(:,:,n)');
        
        end
        
        % Compute LL after optimization (should always increase in the
        % conventional EM algorithm)
        LLTopt = LLTopt - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
            trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
            trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));

    end
    
    LL(k) = LL(k) + LLT; 
    
    LLTopt = sum(sum(S1.*log(PI))) + LLTopt;
    display(['PSEM iteration ',num2str(k),'. Increase in LL: ', num2str(LLTopt - LL(k))])

    Ahist(:,:,:,k+1) = A;
	Bhist(:,:,:,k+1) = B;
    Chist(:,:,:,k+1) = C;
    Qhist(:,:,:,k+1) = Q;
    Rhist(:,:,:,k+1) = R;
    lnThist(:,:,k+1) = log(PI.');
    
%     toc
end

res.Ahist = Ahist;
res.Bhist = Bhist;
res.Chist = Chist;
res.Qhist = Qhist;
res.Rhist = Rhist;
res.lnThist = lnThist;

display('Done!')

end