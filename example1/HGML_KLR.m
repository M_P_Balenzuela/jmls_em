function [zred,Lzred, szred, rzred] = HGML_KLR(z,L, s, r, epsilon,minPerCompatible)
% Reduce a Hybrid GM likelihood down to one of fewer components using KL reduction

% % % IN:
% %     % z(i) - Discrete state of i-th mode
% %     % lnw(i) - Log-weight of i-th mode
% %     % mu(:,i) - Mean of i-th mode
% %     % P(:,:,i) - Covariance of i-th mode
% %     % Mu - Maximum number of modes per discrete variable in reduced mixture
% % 
% % % OPTIONAL INPUTS:
% % 	% Ml - Minimum number of components after reduction per discrete state (may be fewer if input
% % 	% GM has less components for that discrete state)
% % 	% lambda - Error threshold, used for deturmining if mixture should have
% % 	% fewer than Mu elements
% % 
% % % OUT:
% % 	% nummodes - Number of components in reduced mixture
% %     % zred(i) - Discrete state of the i-th mode in the mixture
% % 	% lnwzred(i) - Log-weight of i-th mode in reduced mixture
% % 	% muzred(:,i) - Mean of i-th mode in reduced mixture
% % 	% Pzred(:,:,i) - Covariance of i-th mode in reduced mixture



% Get number of continious and discrete states
minz = min(z);
maxz = max(z);
% nz = maxz-minz+1;

[nx,~,Mmax] = size(L);

% pre-allocate space
zred = nan(Mmax,1);
rzred = nan(Mmax,1);
szred = nan(nx,Mmax);
Lzred = nan(nx,nx,Mmax);

nummodes = 0;
for zi = minz:maxz
    idxs = find(zi==z);
    
    rz = r(idxs);
    sz = s(:,idxs);
    Lz = L(:,:,idxs);

    if (nx == 1)
    
        [Mz, Lz,sz,rz] = GML_KLR_singleState(Lz,sz,rz, epsilon,minPerCompatible);
    else

    	[Mz, Lz,sz,rz] = GML_KLR(Lz,sz,rz, epsilon,minPerCompatible);
    %[Mz, lnwz, muz, Pz] = GM_reduction_KL(lnwz, muz, Pz, Mu, Ml, lambda);
    end

    zred(nummodes+1:nummodes+Mz) = zi;
    rzred(nummodes+1:nummodes+Mz) = rz;
    szred(:,nummodes+1:nummodes+Mz) = sz;
    Lzred(:,:,nummodes+1:nummodes+Mz) = Lz;
    nummodes=nummodes+Mz;
end

zred = zred(1:nummodes);
rzred = rzred(1:nummodes);
szred = szred(:,1:nummodes);
Lzred = Lzred(:,:,1:nummodes);