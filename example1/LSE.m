function wout = LSE(win)
% vectorwise LSE trick along the first dimension of a matrix (each column). 
wstar = max(win);
wout = wstar + log(sum(exp(win-wstar)));