function [ res ] = RB_PSAEM_JMLGSS( sys, init, opt )
%RB_PSAEM_JMLGSS PSAEM for jump Markov linear (JMLGSS) models.
%   Rao-Blackwellized PSAEM for identification for models on the form
%
%   s(t+1) ~ p(s(t+1)|s(t)) = PI(s(t),s(t+1))
%   z(t+1) = A(s(t+1))*z(t) + B(s(t+1))*u(t) + w(t)
%   y(t)   = C(s(t))zx(t)                    + e(t)
%
%   s(t) belongs to {1, ..., sys.K} for all t, and
%   E[w(t)'*w(t)] = Q(s(t+1))
%   E[e(t)'*e(t)] = R(s(t+1))
%   E[w(t)'*e(t)] = 0.
% 
%   length(z(t)) = nz
%   length(u(t)) = nu
%   length(y(t)) = ny
% 
%   Struct sys must also contain measurements sys.Y, length of sys.Y as sys.T
%   and inputs sys.U.
%
%   Struct init must contain initial guesses of A, B, C, Q, R, PI.
%
%   Struct opt must contain options for PSAEM, as opt.gamma (sequence for
%   stochastic approximation), opt.num_iter (number of iterations),
%   opt.N (number of particles).
%
%   Reference: Andreas Svensson, Thomas B. Sch�n and Fredrik Lindsten.
%   Identification of jump Markov linear models using particle filters.
%   Submitted to the 53rd IEEE Conference on Decision and Control (CDC),
%   Los Angeles, CA, USA, December, 2014.

% Intitialization and pre-allocation
A = init.A; B = init.B; C = init.C; Q = init.Q; R = init.R; PI = init.PI;
P0 = sys.P0; z0 = sys.z0; N = opt.N; num_iter = opt.num_iter; gamma = opt.gamma;
K = sys.K; nz = sys.nz; nu = sys.nu; ny = sys.ny; T = sys.T; Y = sys.Y; U = sys.U;
Ahist = zeros(nz,nz,K,num_iter+1); Bhist = zeros(nz,nu,K,num_iter+1);
Chist = zeros(ny,nz,K,num_iter+1); Qhist = zeros(nz,nz,K,num_iter+1);
Rhist = zeros(ny,ny,K,num_iter+1); Ahist(:,:,:,1) = A; Bhist(:,:,:,1) = B;
Chist(:,:,:,1) = C; Qhist(:,:,:,1) = Q; Rhist(:,:,:,1) = R;
S1 = zeros(K,K); S2 = zeros(K,1); LL = zeros(1,num_iter);
S3 = zeros(3*nz+nu+ny,3*nz+nu+ny,K);
lnThist = zeros(K,K,num_iter+1); 
lnThist(:,:,1) = log(PI.');

s_prim = 1*ones(T,1);

for k = 1:num_iter
%     tic;
    
    % Pre-allocate
    w = zeros(N,T); s_pf = zeros(T,N);
    z_kf = zeros(nz,T,N); z_kp = zeros(nz,T,N);
    y_kp = zeros(ny,T+1,N); Kf = zeros(nz,ny,T,N);
    P_kf = zeros(nz,nz,T,N); P_kp = zeros(nz,nz,T,N);
    Py_kp = zeros(ny,ny,T+1,N);

    % Initialize
    s_pf(:,end) = s_prim; z_kp(:,1,:) = repmat(z0,[1,1,N]);
    P_kp(:,:,1,:) = repmat(P0,[1,1,1,N]);
    w(:,1) = ones(size(w(:,1))); w(:,1) = w(:,1)./sum(w(:,1));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % R-B CPF with ancestor sampling %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Compute lambda, omega for conditional trajectory
    CRC = zeros(nz,nz,K);
    F = zeros(nz,nz,K);
    for n = 1:K
        CRC(:,:,n) = (C(:,:,n)'/R(:,:,n))*C(:,:,n);
        F(:,:,n) = chol(Q(:,:,n));
    end
    
    omega_t = zeros(nz,nz,T-1);
    lambda_t = zeros(nz,T-1);
    
    lambda_hat = (C(:,:,s_pf(T,end))'/R(:,:,s_pf(T,end)))*Y(:,T);
    omega_hat = CRC(:,:,s_pf(T,end));
    for t = T-1:-1:1
        Mtp1 = F(:,:,s_pf(t+1,end))'*omega_hat*F(:,:,s_pf(t+1,end)) + eye(nz);
        ApIetc = A(:,:,s_pf(t+1,end))'*(eye(nz)-omega_hat*(F(:,:,s_pf(t+1,end))/Mtp1)*F(:,:,s_pf(t+1,end))');
        omega_t(:,:,t) = ApIetc*omega_hat'*A(:,:,s_pf(t+1,end));
        lambda_t(:,t) = ApIetc*(lambda_hat-omega_hat*B(:,:,s_pf(t+1,end))*U(:,t));
        
        omega_hat = omega_t(:,:,t) + CRC(:,:,s_pf(t,end));
        lambda_hat = lambda_t(:,t) + (C(:,:,s_pf(t,end))'/R(:,:,s_pf(t,end)))*Y(:,t);
    end
    
    % Generate first sample
    [s_pf(1,1:end-1),~] = find(mnrnd(1,ones(N-1,K)./K)');
    
    for t = 1:T
        
        % PF time propagation and ancestor sampling
        if t >= 2
            [a,~] = find(mnrnd(1,repmat(w(:,t-1)',N-1,1))');
            [s_pf(t,1:end-1),~] = find(mnrnd(1,PI(s_pf(t-1,a),:))');
            lam = zeros(nz,nz,N);
            eta = zeros(N,1);
            logw_rb = zeros(N,1);
            
            for i = 1:N
                gam = chol(P_kf(:,:,t-1,i));
                lam(:,:,i) = gam'*omega_t(:,:,t-1)*gam + eye(nz);
                eta(i) = (z_kf(:,t-1,i)'*omega_t(:,:,t-1))*z_kf(:,t-1,i) - 2*lambda_t(:,t-1)'*z_kf(:,t-1,i) - ...
                    ((gam'*(lambda_t(:,t-1)-omega_t(:,:,t-1)*z_kf(:,t-1,i)))'*lam(i))*(gam'*(lambda_t(:,t-1)-omega_t(:,:,t-1)*z_kf(:,t-1,i)));
                logw_rb(i) = -eta(i)/2 - log(det(lam(:,:,i)))/2;
            end
            
            logw_rb = logw_rb - max(logw_rb); w_rb = exp(logw_rb);
            waN = w(:,t-1).*w_rb.*PI(sub2ind(size(PI),squeeze(s_pf(t-1,:))',repmat(s_pf(t,end),1,N)'));
            waN = waN./sum(waN); aN = find(mnrnd(1,waN')); s_pf(1:t-1,:) = s_pf(1:t-1,[a;aN]);
            z_kp(:,1:t-1,:) = z_kp(:,1:t-1,[a;aN]); P_kp(:,:,1:t-1,:) = P_kp(:,:,1:t-1,[a;aN]);
            z_kf(:,1:t-1,:) = z_kf(:,1:t-1,[a;aN]); P_kf(:,:,1:t-1,:) = P_kf(:,:,1:t-1,[a;aN]);
            y_kp(:,1:t-1,:) = y_kp(:,1:t-1,[a;aN]); Py_kp(:,:,1:t-1,:) = Py_kp(:,:,1:t-1,[a;aN]);
        end
       
        % Kalman filter
        for i = 1:N
            % Prediction
            if t >= 2
                z_kp(:,t,i)   = A(:,:,s_pf(t,i))*z_kf(:,t-1,i) + B(:,:,s_pf(t,i))*U(:,t-1);
                P_kp(:,:,t,i) = A(:,:,s_pf(t,i))*P_kf(:,:,t-1,i)*A(:,:,s_pf(t,i))' + Q(:,:,s_pf(t,i));
            end
            
            % Filtering
            Kf(:,:,t,i) = P_kp(:,:,t,i)*C(:,:,s_pf(t,i))'/(C(:,:,s_pf(t,i))*P_kp(:,:,t,i)*C(:,:,s_pf(t,i))'+R(:,:,s_pf(t,i)));
            z_kf(:,t,i) = z_kp(:,t,i) + Kf(:,:,t,i)*(Y(:,t)-C(:,:,s_pf(t,i))*z_kp(:,t,i));
            P_kf(:,:,t,i) = P_kp(:,:,t,i) - Kf(:,:,t,i)*C(:,:,s_pf(t,i))*P_kp(:,:,t,i);
            
            % Prediction of y
            y_kp(:,t,i) = C(:,:,s_pf(t,i))*z_kp(:,t,i);
            Py_kp(:,:,t,i) = C(:,:,s_pf(t,i))*P_kp(:,:,t,i)*C(:,:,s_pf(t,i))' + R(:,:,s_pf(t,i));
        end
        
        % PF weight update
        Py_temp = zeros(ny,ny,N); Py_temp(:,:,:) = Py_kp(:,:,t,:);
        if ny > 1
            y_kp_temp = squeeze(y_kp(:,t,:))';
        else
            y_kp_temp = squeeze(y_kp(:,t,:));
        end
        w(:,t) = mvnpdf(repmat(Y(:,t),1,N)',y_kp_temp,Py_temp)+eps;
        w(:,t) = w(:,t)/sum(w(:,t));
    end
    
    
    %%%%%%%%%%%%%%%%
    % RTS smoother %
    %%%%%%%%%%%%%%%%
    
    % Pre-allocating
    z_ks = zeros(nz,T+1,N); P_ks = zeros(nz,nz,T+1,N);
    J = zeros(nz,nz,T,N); M = zeros(nz,nz,T,N);
    
    for i = 1:N
        % Inititalizing
        P_ks(:,:,end,i) = P_kp(:,:,end,i); P_ks(:,:,end-1,i) = P_kf(:,:,end,i);
        z_ks(:,end,i) = z_kp(:,end,i); z_ks(:,end-1,i) = z_kf(:,end,i);
        
        % Smoothing        
        for t = T-1:-1:1
            J(:,:,t,i) = P_kf(:,:,t,i)*A(:,:,s_pf(t+1,i))'/P_kp(:,:,t+1,i);
            z_ks(:,t,i) = z_kf(:,t,i)+J(:,:,t,i)*(z_ks(:,t+1,i)-z_kp(:,t+1,i));
            P_ks(:,:,t,i) = P_kf(:,:,t,i) + J(:,:,t,i)*(P_ks(:,:,t+1,i)-P_kp(:,:,t+1,i))*J(:,:,t,i)';
        end
        
        % Computing of M = P_{t-1,t|T}
        M(:,:,end,i) = (eye(nz)-Kf(:,:,T,i)*C(:,:,s_pf(T,i)))*A(:,:,s_pf(T,i))*P_kf(:,:,T-1,i);
        for t = T-1:-1:2
            M(:,:,t,i) = P_kf(:,:,t,i)*J(:,:,t-1,i)' + J(:,:,t,i)*(M(:,:,t+1,i)-A(:,:,s_pf(t+1,i))*P_kf(:,:,t,i))*J(:,:,t-1,i)';
        end
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sample trajectory to condition on %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    star = logical(mnrnd(1,w(:,end))); s_prim = squeeze(s_pf(:,star));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Compute and update sufficient statistics %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    S1T = zeros(K,K); S2T = zeros(K,1);
	S3T = zeros(3*nz+nu+ny,3*nz+nu+ny,K);
    
    for n = 1:K
        % S1
        for m = 1:K
            for i = 1:N
                S1T(n,m) = S1T(n,m) + w(i,end).*numel(strfind(s_pf(:,i)',[n m]));
            end
            S1T(n,m) = 1/N*S1T(n,m);
        end
        
        % S2
        S2T(n) = sum(sum(s_pf(2:end,:)==n)'.*w(:,end));
        
        % S3
        for i = 1:N
            for t = 2:1:T
                xi_t_hat = [z_ks(:,t,i); z_ks(:,t-1,i); U(:,t-1); Y(:,t); z_ks(:,t,i)];
                M_t = [P_ks(:,:,t,i), M(:,:,t,i),      zeros(nz,nu+ny),P_ks(:,:,t,i);...
                       M(:,:,t,i),    P_ks(:,:,t-1,i), zeros(nz,nu+ny),M(:,:,t,i);...
                       zeros(nu+ny,3*nz+nu+ny);
                       P_ks(:,:,t,i), M(:,:,t,i),      zeros(nz,nu+ny),P_ks(:,:,t,i)];
                S3T(:,:,n) = S3T(:,:,n) + w(i,end) .* (s_pf(t,i)==n) * (xi_t_hat*xi_t_hat' + M_t);
            end

        end
    end
    
    % 'SAEM'-update of suff stat
    S1 = (1-gamma(k)).*S1 + gamma(k).*S1T + eps;
    S2 = (1-gamma(k)).*S2 + gamma(k).*S2T;
    S3 = (1-gamma(k)).*S3 + gamma(k).*S3T;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute log likelihood %
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLT = 0; LLTopt = 0; LL(k) = sum(sum(S1T.*log(PI)));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Estimate new parameters %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for n = 1:K
        
        Phi     = S3(1:nz,1:nz,n);
        Psi     = S3(1:nz,nz+(1:nz+nu),n);
        Sigma   = S3(nz+(1:nz+nu),nz+(1:nz+nu),n);

        Xi      = S3(2*nz+ny+nu+(1:nz),2*nz+ny+nu+(1:nz),n);
        Lambda  = S3(2*nz+nu+(1:ny),2*nz+ny+nu+(1:nz),n);
        Omega   = S3(2*nz+nu+(1:ny),2*nz+nu+(1:ny),n);
        
        % Part of LL computation
        LLT = LLT - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
            trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
            trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));
        
        PI(n,:) = S1(n,:)./sum(S1(n,:));

        AB = Psi/Sigma;
        CD = Lambda/Xi;

        A(:,:,n) = AB(:,1:nz);                       
        B(:,:,n) = AB(:,nz+1:end);                   
        C(:,:,n) = CD(:,1:nz);                       
        Q(:,:,n) = (Phi - (Psi/Sigma)*Psi')/S2(n);      
        R(:,:,n) = (Omega - (Lambda/Xi)*Lambda')/S2(n); 
        

            Q(:,:,n) = max(Q(:,:,n),0.0001);
            R(:,:,n) = max(R(:,:,n),0.0001);
            
        if min(eig(Q(:,:,n)))< 0 % Numerical fix
            Q(:,:,n) = Q(:,:,n) + 0.001*eye(nz);
        end
        
        % Compute LL after optimization (should always increase in the
        % conventional EM algorithm)
        LLTopt = LLTopt - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
            trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
            trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));

    end
    
    LL(k) = LL(k) + LLT;
    
    LLTopt = sum(sum(S1T.*log(PI))) + LLTopt;
    display(['RB-PSAEM iteration ',num2str(k),'. Increase in LL: ', num2str(LLTopt - LL(k))])

    Ahist(:,:,:,k+1) = A;
	Bhist(:,:,:,k+1) = B;
    Chist(:,:,:,k+1) = C;
    Qhist(:,:,:,k+1) = Q;
    Rhist(:,:,:,k+1) = R;
    lnThist(:,:,k+1) = log(PI.');
    
%     toc
end

res.Ahist = Ahist;
res.Bhist = Bhist;
res.Chist = Chist;
res.Qhist = Qhist;
res.Rhist = Rhist;
res.lnThist = lnThist;

display('Done!')

end

