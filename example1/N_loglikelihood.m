function L = N_loglikelihood(x,mu,P)
%Calculate the normal loglikelihood ln(N(x|mu,P))

% IN:
    % P - Covariance for all samples
    % mu - mean of the normal distribution
    % x(:,i) - value of the random variable to be evaluated against the dist.

% OUT: 
    % L(i) - log-likelihood corresponding to point x(:,i)
    

nx = length(P);
es = x-mu;

% Numerically terrible way
% L = -0.5*log(det(2*pi*P)) -diag(0.5*es'/P*es);

% Better Maths intensive way
cholP = chol(P);
A = diag(cholP);
A = log(A);
L = sum(A);

es = es';
es = es/cholP;
es = es .* es;
B = sum(es,2);
B = B + nx*log(2*pi);
B = 0.5*B;
L = L + B;
L = -L;
