function JMLS_EM_plot(itt,JMLS_actual,JMLS,JMLS_old,old_lnL,lnL, colours)

    nz = JMLS.N;
    
    if itt > 1
        figure(1);
        hold on
        plot([itt-2 itt-1], [old_lnL lnL],colours{2*nz+3});
        title('ln(p(y))')
    end
    
    figure(2);
    subplot(4,1,1);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'A',colours)
    title('A');
    
    subplot(4,1,2);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'B',colours)
    title('B');    

    subplot(4,1,3);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'C',colours)
    title('C');    
    
    subplot(4,1,4);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'D',colours)
    title('D');    
    
    
    figure(3);
    subplot(2,1,1);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'Q',colours)
    title('Q'); 
 
    subplot(2,1,2);
    hold on;
    plot_JMLS_qty(itt,nz,JMLS_actual,JMLS,JMLS_old,'R',colours)
    title('R'); 
    

    
    
    figure(4);
    subplot(2,1,1);
    for i=1:nz
        plot([itt-1 itt],exp(JMLS_actual.lnT(i,i)) *ones(2,1), colours{i})
        hold on;
    end

    for i=1:nz
        plot([itt-1 itt],[exp(JMLS_old.lnT(i,i)) exp(JMLS.lnT(i,i))], colours{i+nz})    
    end

    title(['T_{i,i}']);

    subplot(2,1,2);
    for i=1:nz
        for j = 1:nz
            if i ~=j
            plot([itt-1 itt],exp(JMLS_actual.lnT(j,i)) *ones(2,1), colours{2*nz+1})
            hold on;
            end
        end
    end

    for i=1:nz
        for j = 1:nz
            if i ~=j
            plot([itt-1 itt],[exp(JMLS_old.lnT(j,i)) exp(JMLS.lnT(j,i))], colours{2*nz+2})
            end
        end
    end
        title(['T_{i,j~=i}']);



    drawnow;
end

function plot_JMLS_qty(itt, nz,JMLS_actual,JMLS,JMLS_old,qty,colours)

    for m = 1:nz
        plot([itt-1 itt],getfield(JMLS_actual.model(m),qty) *ones(2,1), colours{m});
    end

    for m = 1:nz
        plot([itt-1 itt],[getfield(JMLS_old.model(m),qty) getfield(JMLS.model(m),qty)], colours{m+nz})
    end
end