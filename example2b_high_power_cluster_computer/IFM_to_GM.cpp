#include "mex.h"
#include "math.h"
#include <stdlib.h>

#include "Eigen/Dense"
using namespace Eigen;

#define LOG2PI 1.8378770664093453390819377091248 //ln(2.0*pi), used "vpa(log(2.0*pi))" in matlab R2018a



/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
/*
[P_arr,mu_arr,lnw_arr] = IFM_to_GM(L_arr,s_arr,r_arr)
% Convert a Gaussian mixture distribution into Information form mixture
*/
	
// Get input pointers	
double *L_arr = mxGetPr(prhs[0]);
double *s_arr = mxGetPr(prhs[1]);
double *r_arr = mxGetPr(prhs[2]);

int M = (int) mxGetN(prhs[1]); // Number of initial modes
int nx =(int) mxGetM(prhs[1]); // get state dimension

double *P_arr, *mu_arr, *lnw_arr;
mwSize dims2[3] = {(mwSize)nx,(mwSize) nx,(mwSize) M};
plhs[0] = mxCreateNumericArray(3,dims2,mxDOUBLE_CLASS,mxREAL);
P_arr = mxGetPr(plhs[0]);
	
plhs[1] = mxCreateDoubleMatrix(nx,M,mxREAL);
mu_arr = mxGetPr(plhs[1]);

bool scalar_stats = false;
if (nrhs >2)
{
	r_arr = mxGetPr(prhs[2]);
	plhs[2] = mxCreateDoubleMatrix(M,1,mxREAL);
	lnw_arr = mxGetPr(plhs[2]);
	scalar_stats = true;
}

int m;
int nx2 = nx*nx;
for (m=0;m<M;++m)
{
	
	Map<MatrixXd> mu_m(&mu_arr[m*nx], nx, 1);
	Map<MatrixXd> s_m(&s_arr[m*nx], nx, 1);
	Map<MatrixXd> P_m(&P_arr[m*nx2], nx, nx);
	Map<MatrixXd> L_m(&L_arr[m*nx2], nx, nx);
	
	P_m = L_m.inverse();
	mu_m = -P_m*s_m;
	
	if (scalar_stats)
	{
		double tmp = nx*LOG2PI + log(P_m.determinant())- r_arr[m];
		Map<MatrixXd> etmp(&tmp, 1, 1);
		MatrixXd lnw_m = 0.5*( mu_m.transpose()*L_m*mu_m + etmp);
		lnw_arr[m] = *lnw_m.data();
	}

}

}
