function [lnw_f,mu_f,P_f,z_f,LnL] = JMLS_filter(lnwf,muf,Pf,zf,JMLS,ym,u)

if nargin < 7
    u = zeros(0,1);
end

nz = JMLS.N;
[ny,nx] = size(JMLS.model(1).C);

Nf = length(lnwf);
zerony = zeros(ny,1);

N_f = Nf*nz;

z_f = nan(N_f,1);
lnw_f = nan(N_f,1);
mu_f = nan(nx,N_f);
P_f = nan(nx,nx,N_f);

% Discrete prediction
for z = 1:nz
    sidx = Nf*(z-1)+1;
    eidx = z*Nf;
    z_f(sidx:eidx) = repmat(z,Nf,1); 
    
    [A,b,Q,C,d,R] = LG_unpack(JMLS.model(z),u);    
    
    mup = A*muf + b;
    es = C*mup + d - repmat(ym,1,Nf);
    for i = 1:Nf
        sidxi = sidx + i-1;
        Pp = A*Pf(:,:,i)*A.' + Q;
        Py = C*Pp*C.' + R;  
        Kg = Pp*C.'/Py;
        mu_f(:,sidxi) = mup(:,i) - Kg*es(:,i); 
        P_f(:,:,sidxi) = Pp - Kg*Py*Kg.';
        
        tmp = N_loglikelihood(es(:,i),zerony,Py);
        lnw_f(sidxi) = tmp + lnwf(i) + JMLS.lnT(z,zf(i));
    end
end

if nargout > 4
    LnL = LSE(lnw_f);
end

% Normalise the filtered distribution
lnw_f = lnw_normalise(lnw_f);


    