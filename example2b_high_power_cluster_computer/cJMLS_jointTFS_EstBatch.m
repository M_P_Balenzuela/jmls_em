function [JMLSF, JMLSS, lnL] = cJMLS_jointTFS_EstBatch(cJMLS, u_arr,y_arr,prior,KLR_Mu, KLR_epsilon)

% Grab dimensions
[~,N] = size(u_arr);

% Set prior
zf = prior.z;
lnwf = prior.lnw;
muf = prior.mu;
cPf = prior.cP;

JMLSF.dist(1).z = zf;
JMLSF.dist(1).lnw = lnwf;
JMLSF.dist(1).mu = muf;
JMLSF.dist(1).cP = cPf;
    
KLR_Muf = KLR_Mu*cJMLS.N;

lnL = 0;
% Run the JMLS filter over the timeseries
for k = 1:N-1
    u = u_arr(:,k+1);
    ym = y_arr(:,k+1);

    % Run JMLS filter
    [lnwf,muf,cPf,zf,lnLk] = cJMLS_filter(lnwf,muf,cPf,zf,cJMLS,ym,u);
    
    keep_idx = (lnwf > -50);
    lnwf = lnwf(keep_idx);
    muf = muf(:,keep_idx);
    cPf = cPf(:,:,keep_idx);
    zf = zf(keep_idx);
    lnwf = lnw_normalise(lnwf);
    
    lnL = lnL + lnLk;
    
    % KL Reduction on filtered distribution
    [zf, lnwf, muf, cPf] = cHGM_KLR(zf,lnwf, muf, cPf, KLR_Muf);

    % Save Filtered distribution
    JMLSF.dist(k+1).z = zf;
    JMLSF.dist(k+1).lnw = lnwf;
    JMLSF.dist(k+1).mu = muf;
    JMLSF.dist(k+1).cP = cPf;

end


if (nargout > 1)
    % Suppose I should make the smoothed distribution then
    
        zf = JMLSF.dist(N-1).z;
        lnwf = JMLSF.dist(N-1).lnw;
        muf = JMLSF.dist(N-1).mu;
        cPf = JMLSF.dist(N-1).cP;
        
        u = u_arr(:,N);
        ym = y_arr(:,N);       

        
        % Call the joint TF smoother
        [cLc,sc,rc,zc,lnws,mus,cPs,zs] = cJMLS_jointTFS(cJMLS,u,ym,cPf,muf,lnwf,zf);

        
        % Save the first joint smoothed distribution
        JMLSS.dist(N-1).z = zs;
        JMLSS.dist(N-1).lnw = lnws;
        JMLSS.dist(N-1).mu = mus;
        JMLSS.dist(N-1).cP = cPs;       
        
    % Run the joint TF smoother over the timeseries
    for k = N-2:-1:1

        u = u_arr(:,k+1);
        ym = y_arr(:,k+1);
        
        % Load the filtered distribution
        zf = JMLSF.dist(k).z;
        lnwf = JMLSF.dist(k).lnw;
        muf = JMLSF.dist(k).mu;
        cPf = JMLSF.dist(k).cP;
        
        % KL Likelihood reduction
        [zc,cLc, sc, rc] = cHGML_KLR(zc,cLc, sc, rc, KLR_epsilon, KLR_Mu);
        
        % Call the joint TF smoother
        [cLc,sc,rc,zc,lnws,mus,cPs,zs] = cJMLS_jointTFS(cJMLS,u,ym,cPf,muf,lnwf,zf, cLc,sc,rc,zc);
        
        keep_idx = (lnws > -50);
        lnws = lnws(keep_idx);
        mus = mus(:,keep_idx);
        cPs = cPs(:,:,keep_idx);
        zs = zs(:,keep_idx);
        lnws = lnw_normalise(lnws);

        
        % Store the joint Smoothed distribution [xk;xk+1]
        JMLSS.dist(k).z = zs;
        JMLSS.dist(k).lnw = lnws;
        JMLSS.dist(k).mu = mus;
        JMLSS.dist(k).cP = cPs;

    end  
end