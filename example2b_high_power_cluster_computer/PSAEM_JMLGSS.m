function [ res ] = PSAEM_JMLGSS( sys, init, opt )
%PSAEM_JMLGSS PSAEM for jump Markov linear (JMLGSS) models.
%   PSAEM for identification for models on the form
%
%   s(t+1) ~ p(s(t+1)|s(t)) = PI(s(t),s(t+1))
%   z(t+1) = A(s(t+1))*z(t) + B(s(t+1))*u(t) + w(t)
%   y(t)   = C(s(t))zx(t)                    + e(t)
%
%   s(t) belongs to {1, ..., K} for all t, and
%   E[w(t)'*w(t)] = Q(s(t+1))
%   E[e(t)'*e(t)] = R(s(t+1))
%   E[w(t)'*e(t)] = 0.
% 
%   length(z(t)) = nz
%   length(u(t)) = nu
%   length(y(t)) = ny
% 
%   Struct sys must also contain measurements Y, length of Y as sys.T
%   and inputs U.
%
%   Struct init must contain initial guesses of A, B, C, Q, R, PI.
%
%   Struct opt must contain options for PSAEM, as opt.gamma (sequence for
%   stochastic approximation), opt.num_iter (number of iterations),
%   opt.N (number of particles).
%   
%   Reference:
%   F. Lindsten An efficient stochastic approximation EM algorithm using
%   conditional particle filters. Proceedings of the 38th International
%   Conference on Acoustics, Speech, and Signal Processing (ICASSP),
%   Vancouver, Canada, May 2013.

% Intitialization and pre-allocation
A = init.A; B = init.B; C = init.C; Q = init.Q; R = init.R; PI = init.PI;
P0 = sys.P0; z0 = sys.z0; N = opt.N; num_iter = opt.num_iter; gamma = opt.gamma;
K = sys.K; nz = sys.nz; nu = sys.nu; ny = sys.ny; T = sys.T; Y = sys.Y; U = sys.U;
Ahist = zeros(nz,nz,K,num_iter+1); Bhist = zeros(nz,nu,K,num_iter+1);
Chist = zeros(ny,nz,K,num_iter+1); Qhist = zeros(nz,nz,K,num_iter+1);
Rhist = zeros(ny,ny,K,num_iter+1); 
lnThist = zeros(K,K,num_iter+1); 
Ahist(:,:,:,1) = A; Bhist(:,:,:,1) = B;
Chist(:,:,:,1) = C; Qhist(:,:,:,1) = Q; Rhist(:,:,:,1) = R;
S1 = zeros(K,K); S2 = zeros(K,1); LL = zeros(1,num_iter);
S3 = zeros(3*nz+nu+ny,3*nz+nu+ny,K);
lnThist(:,:,1) = log(PI.');

s_prim = 1*ones(T,1);
z_prim = 0*ones(nz,T,1);

for k = 1:num_iter
%     tic;
    
    % Pre-allocate
    w = zeros(N,T); s_pf = zeros(T,N);
    z_pf = zeros(nz,T,N);

    % Initialize
    s_pf(:,end) = s_prim; z_pf(:,:,end) = z_prim;
    w(:,1) = ones(size(w(:,1))); w(:,1) = w(:,1)./sum(w(:,1));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % CPF with ancestor sampling %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Generate first sample
    [s_pf(1,1:end-1),~] = find(mnrnd(1,ones(N-1,K)./K)');
    z_pf(:,1,1:end-1) = mvnrnd(repmat(z0',N-1,1),P0)';
    
    for t = 1:T
        
        % PF time propagation and ancestor sampling
        if t >= 2
            [a,~] = find(mnrnd(1,repmat(w(:,t-1)',N-1,1))');
            [s_pf(t,1:end-1),~] = find(mnrnd(1,PI(s_pf(t-1,a),:))');
            z_pfP1a = zeros(nz,N-1);
            for i = 1:N-1
                z_pfP1a(:,i) = A(:,:,s_pf(t,i))*z_pf(:,t-1,a(i)) + B(:,:,s_pf(t,i))*U(:,t-1);
            end
            z_pf(:,t,1:end-1) = mvnrnd(z_pfP1a',Q(:,:,s_pf(t,1:end-1)))';
            
            lwaN = zeros(1,N);
            for i = 1:N
                z_pfP1c = A(:,:,s_pf(t,end))*z_pf(:,t-1,i) + B(:,:,s_pf(t,end))*U(:,t-1);
                lwaN(i) = log(w(i,t-1)) + log(PI(s_pf(t-1,i),s_pf(t,end))) - 1/2*log(det(Q(:,:,s_pf(t,end)))) - 1/2*((z_pf(:,t,end)- z_pfP1c)'/Q(:,:,s_pf(t,end)))*(z_pf(:,t,end)- z_pfP1c);
            end
            waN = exp(lwaN-max(lwaN)); waN = waN./sum(waN); aN = find(mnrnd(1,waN'));
            s_pf(1:t-1,:) = s_pf(1:t-1,[a;aN]);
            z_pf(:,1:t-1,:) = z_pf(:,1:t-1,[a;aN]);
        end
               
        % PF weight update
        lw = zeros(N,1);
        for i = 1:N
            y_pfP = C(:,:,s_pf(t,i))*z_pf(:,t,i);
            lw(i) = -1/2*log(det(R(:,:,s_pf(t,i)))) - 1/2*((y_pfP-Y(:,t))'/R(:,:,s_pf(t,i)))*(y_pfP-Y(:,t));
        end
        w(:,t) = exp(lw-max(lw));
        w(:,t) = w(:,t)/sum(w(:,t));
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sample trajectory to condition on %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    star = find(mnrnd(1,w(:,end))); s_prim = squeeze(s_pf(:,star));
    z_prim = squeeze(z_pf(:,:,star));
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Compute and update sufficient statistics %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    S1T = zeros(K,K); S2T = zeros(K,1);
	S3T = zeros(3*nz+nu+ny,3*nz+nu+ny,K);
    
    for n = 1:K
        % S1
        for m = 1:K
            for i = 1:N
                S1T(n,m) = S1T(n,m) + w(i,end).*numel(strfind(s_pf(:,i)',[n m]));
            end
            S1T(n,m) = 1/N*S1T(n,m);
        end
        
        % S2
        S2T(n) = sum(sum(s_pf(2:end,:)==n)'.*w(:,end));
        
        % S3
        for i = 1:N
            for t = 2:1:T
                xi_t_hat = [z_pf(:,t,i); z_pf(:,t-1,i); U(:,t-1); Y(:,t); z_pf(:,t,i)];
                S3T(:,:,n) = S3T(:,:,n) + w(i,end) .* (s_pf(t,i)==n) * (xi_t_hat*xi_t_hat');
            end

        end
    end
    
    % 'SAEM'-update of suff stat
    S1 = (1-gamma(k)).*S1 + gamma(k).*S1T + eps;
    S2 = (1-gamma(k)).*S2 + gamma(k).*S2T;
    S3 = (1-gamma(k)).*S3 + gamma(k).*S3T;
    
    if sum(sum(isnan(S3))) > 0
        display('Problems encountered!')
        break
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute log likelihood %
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLT = 0;
    LLTopt = 0;
    LL(k) = sum(sum(S1T.*log(PI)));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Estimate new parameters %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for n = 1:K
        
        Phi     = S3(1:nz,1:nz,n);
        Psi     = S3(1:nz,nz+(1:nz+nu),n);
        Sigma   = S3(nz+(1:nz+nu),nz+(1:nz+nu),n);

        Xi      = S3(2*nz+ny+nu+(1:nz),2*nz+ny+nu+(1:nz),n);
        Lambda  = S3(2*nz+nu+(1:ny),2*nz+ny+nu+(1:nz),n);
        Omega   = S3(2*nz+nu+(1:ny),2*nz+nu+(1:ny),n);
        
        % Part of LL computation
        LLT = LLT - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
            trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
            trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));
        
        PI(n,:) = S1(n,:)./sum(S1(n,:));

        AB = Psi/Sigma;
        CD = Lambda/Xi;

        A(:,:,n) = AB(:,1:nz);
        B(:,:,n) = AB(:,nz+1:end);
        C(:,:,n) = CD(:,1:nz);
        Q(:,:,n) = (Phi - (Psi/Sigma)*Psi')/S2(n);
        R(:,:,n) = (Omega - (Lambda/Xi)*Lambda')/S2(n);
        
        Q(:,:,n) = 1/2*(Q(:,:,n) + Q(:,:,n)');
        R(:,:,n) = 1/2*(R(:,:,n) + R(:,:,n)');
        
        % Compute LL after optimization (should always increase in the
        % conventional EM algorithm)
        LLTopt = LLTopt - 1/2*(S2(n)*log(det(Q(:,:,n))*det(R(:,:,n))) + ...
            trace(Q(:,:,n)\(Phi - Psi*[A(:,:,n) B(:,:,n)]' - [A(:,:,n) B(:,:,n)]*Psi' + [A(:,:,n) B(:,:,n)]*Sigma*[A(:,:,n) B(:,:,n)]')) + ...
            trace(R(:,:,n)\(Omega - Lambda*C(:,:,n)' - C(:,:,n)*Lambda' + C(:,:,n)*Xi*C(:,:,n)')));

    end
    
    LL(k) = LL(k) + LLT; %subplot(3,1,3); plot(LL); title('Log likelihood'),%display(PI)
    
    LLTopt = sum(sum(S1T.*log(PI))) + LLTopt;
    display(['PSAEM iteration ',num2str(k),'. Increase in LL: ', num2str(LLTopt - LL(k))])

    Ahist(:,:,:,k+1) = A;
    Bhist(:,:,:,k+1) = B;
    Chist(:,:,:,k+1) = C;
	Qhist(:,:,:,k+1) = Q;
    Rhist(:,:,:,k+1) = R;
    lnThist(:,:,k+1) = log(PI.');
%     toc
end

res.Ahist = Ahist;
res.Bhist = Bhist;
res.Chist = Chist;
res.Qhist = Qhist;
res.Rhist = Rhist;
res.lnThist = lnThist;

display('Done!')

end