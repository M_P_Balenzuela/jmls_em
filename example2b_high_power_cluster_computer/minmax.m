function ret = minmax(x)
actual =[max(x);min(x)]
ret = [max(x)-mean(x);-min(x)+mean(x)];% inverse of hardcoded function in shaded error bar