clear
close all
clc

%first we load all the data and cat the results
arx_lnL_JMLSEM = [];
arx_lnL_RB_PSAEM = [];
for i=1:100
   %try
       fn = dir(['long_run_' num2str(i) '_*.mat']);
       dd = load(fn.name);
       arx_lnL_JMLSEM = cat(2,arx_lnL_JMLSEM,dd.arx_lnL_JMLSEM);
       arx_lnL_RB_PSAEM = cat(2,arx_lnL_RB_PSAEM,dd.arx_lnL_RB_PSAEM);
   %catch 
   %end
end
num_iter = size(arx_lnL_JMLSEM,1);

figure(1);
plot(arx_lnL_JMLSEM,'b')
hold on
plot(arx_lnL_RB_PSAEM,'r')



figure;
linethick = 2;

% figure(2);clf;
% timerr = time(4)/num_iter*(0:num_iter);

s = ':';
RGB =['f0'; '02'; '7f'];% magenta
RGBi = hex2dec(RGB)/255;
w=2;
%         plot(x,y,s,'Color',RGBi,'LineWidth',w);

timedelta = dd.time(4)/dd.num_iter*(1:num_iter);
sec_errhandles1 = shadedErrorBar(timedelta, arx_lnL_JMLSEM.', {@mean,@minmax}, 'lineprops', {s,'Color',RGBi,'LineWidth',w});
hold on;
%timerr = time(1)/num_iter*(0:num_iter);

s = '-o';
RGB =['fd'; 'c0'; '86'];%orange
w = 2;
RGBi = hex2dec(RGB)/255;
timedelta = dd.time(1)/dd.num_iter*(1:num_iter);
sec_errhandles2 = ModshadedErrorBar(timedelta, arx_lnL_RB_PSAEM.', {@mean,@minmax}, 'lineprops', {s,'Color',RGBi,'LineWidth',w});

% Bring mean and edge lines to top


uistack(sec_errhandles2.mainLine,'top');
uistack(sec_errhandles2.edge,'top');

uistack(sec_errhandles1.mainLine,'top');
uistack(sec_errhandles1.edge,'top');

legend([sec_errhandles1.mainLine, sec_errhandles1.patch,sec_errhandles2.mainLine, sec_errhandles2.patch], 'Proposed method mean likelihood','Proposed method variation','RB-PSAEM mean likelihood','RB-PSAEM variation');
ylabel('ln(p(y))')
xlabel('Computational time [s]')
title('Log-likelihood vs. Computational time')
 axis([0 50010 -5720 -5646])
% ylim([-5720 -5646]);