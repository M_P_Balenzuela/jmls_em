#!/bin/bash
#
#PBS -l select=1:ncpus=1:mem=8gb
#PBS -l walltime=48:00:00
#PBS -l software=matlab
#PBS -k oe

source /etc/profile.d/modules.sh
module load matlab/R2019b


cd $PBS_O_WORKDIR

#matlab -singleCompThread -nodisplay -r "mex -O GM_KLR.cpp; mex -O GM_to_IFM.cpp; mex -O IFM_to_GM.cpp; mex -O cGM_KLR.cpp; run_me; exit;"
matlab -singleCompThread -nodisplay -r "run_me($RUN); exit;"

