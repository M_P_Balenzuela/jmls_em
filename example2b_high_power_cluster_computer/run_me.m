function run_me(number)
rng(1);
%%%%%%%%%%%%%%%%%%%
% Some parameters %
%%%%%%%%%%%%%%%%%%%
sys.K = 2;              % Number of modes
sys.nz = 2;             % State dimension
sys.nu = 1;             % Input dimension
sys.ny = 1;             % Output dimension
sys.T = 7000;%7           % Number of simulated data points
W = 0.4;                  % Relative disturbance of initial guesses
N1 = 25;               % Number of particles in RB-PSAEM
N2 = 118;                % Number of particles in PSAEM
N3 = 75; M3 = 34 ;      % Number of particles and backward trajectories in PSEM
num_iter =500%750      % Number of iterations
kappa = 1; b = 100;      % Tuning paramters for SA sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%

true_sys.A = zeros(sys.nz,sys.nz,sys.K); true_sys.C = zeros(sys.ny,sys.nz,sys.K);
true_sys.B = zeros(sys.nz,sys.nu,sys.K); true_sys.D = zeros(sys.ny,sys.nz,sys.K);
true_sys.Q = zeros(sys.nz,sys.nz,sys.K); true_sys.R = zeros(sys.ny,sys.ny,sys.K);
true_sys.PI = 0.9*eye(sys.K) + 0.05/sys.K*ones(sys.K);
true_sys.sys.z0 = 0; true_sys.sys.P0 = eye(sys.nz);
sys.z0 = zeros(sys.nz,1); sys.P0 = eye(sys.nz);

gamma = zeros(1,num_iter);gamma(1:2) = 1; gamma(3:b) = 0.95;
gamma(b+1:end) = 0.95*(((0:num_iter-b-1)+kappa)/kappa).^(-0.7);

rb_psaem_opt.N = N1; rb_psaem_opt.gamma = gamma; rb_psaem_opt.num_iter = num_iter;
psaem_opt.N = N2; psaem_opt.gamma = gamma; psaem_opt.num_iter = num_iter;
psem_opt.N = N3; psem_opt.M = M3; psem_opt.num_iter = num_iter;

% Generate system and data
for n = 1:sys.K
    true_sys.A(:,:,n) = diag(2*rand(1,sys.nz)-1);
    true_sys.B(:,:,n) = 5*rand(sys.nz,sys.nu);
    true_sys.C(:,:,n) = 5*rand(sys.ny,sys.nz);
    true_sys.Q(:,:,n) = eye(sys.nz)*(0.01 + 0.09*rand);
    true_sys.R(:,:,n) = eye(sys.ny)*(0.01 + 0.09*rand);
    temp = rand(1,sys.K);
    true_sys.PI(n,:) = true_sys.PI(n,:) + 0.05*temp/sum(temp);
end

true_sys.PI = [0.6 0.5;
    0.4 0.5].';
%
%   true_sys.PI = [0.6 0.3;
%                 0.4 0.7].';

true_sys.A(:,:,1) = [-0.3089  -0.5857;
    0.5857  -0.3089];

true_sys.B(:,:,1) = [-0.2033
    -0.628];

true_sys.C(:,:,1) = [-0.8965   -0.889];

true_sys.A(:,:,2) = [0.604  -0.2859;
    -0.2859   0.5849];
true_sys.B(:,:,2) = [-2.288
    -0.8714];
true_sys.C(:,:,2) = [0  1.677];


true_sys.Q(:,:,1) = diag([0.045 0.03]);
true_sys.Q(:,:,2) = diag([0.032 0.02]);

true_sys.R(:,:,1) = 0.002;
true_sys.R(:,:,2) = 0.005;

sys.U = randn(sys.nu,sys.T);

% usys = zpk([],[-0.01 -2 -3],1); % For generating a lowpass-filtered input signal sys.U
% for n = 1:sys.nu
%     temp = randn(1,sys.T); sys.U(n,:) = lsim(usys,temp,0:sys.T-1)';
% end

S = zeros(1,sys.T+1); Z = zeros(sys.nz,sys.T+1);
Z(:,1) = 0; sys.Y = zeros(sys.ny,sys.T); S(1) = 1;
for t = 1:sys.T
    S(t+1) = find(rand<=cumsum(true_sys.PI(S(t),:)),1);
    Z(:,t+1) = true_sys.A(:,:,S(t+1))*Z(:,t) + true_sys.B(:,:,S(t+1))*sys.U(:,t) + mvnrnd(zeros(sys.nz,1),true_sys.Q(:,:,S(t+1)))';
    sys.Y(:,t) = true_sys.C(:,:,S(t))*Z(:,t) + mvnrnd(zeros(sys.ny,1),true_sys.R(:,:,S(t)))';
end


init.A(:,:,1) = [-0.06993  -0.02433;
    -0.02433  -0.09378];

init.B(:,:,1) = [1.813
    -0.9379];

init.C(:,:,1) = [1.53  0];

init.A(:,:,2) = [0.5581  -0.1922;
    -0.1922   0.7987];

init.B(:,:,2) = [0.2615
    1.112];

init.C(:,:,2) = [-0.007845     -0.981 ];


init.Q(:,:,1) = diag([3.5 1.2]);
init.Q(:,:,2) = diag([1.3 2.0]);

init.R(:,:,1) = 0.05;
init.R(:,:,2) = 0.025;


init.PI = ones(sys.K)/sys.K;





numruns = 1;
arx_lnL_RB_PSAEM = nan(num_iter+1,numruns);
arx_lnL_PSAEM = nan(num_iter+1,numruns);
arx_lnL_PSEM = nan(num_iter+1,numruns);
arx_lnL_JMLSEM = nan(num_iter+1,numruns);

rng(number+100)

for inumruns = 1:numruns
    
    
    init.A = true_sys.A.*(0.8+0.4*rand(1));
    init.B = true_sys.B.*(0.8+0.4*rand(1));
    init.C = true_sys.C.*(0.8+0.4*rand(1));
    init.Q = true_sys.Q.*(0.8+0.4*rand(1));
    init.R = true_sys.R.*(0.8+0.4*rand(1));
    init.PI = ones(sys.K)/sys.K;
    
    
    
    
    
    JMLSEMopt.KLR_Mu = 3;
    JMLSEMopt.KLR_epsilon = 10^-10;
    JMLSEMopt.num_iter = num_iter;
    tic
    JMLSEM_res = cJMLS_EM_wrap( sys, init, JMLSEMopt);
    time(4) = toc;
    
    
    % tic
    % PSEM_res = PSEM_JMLGSS( sys, init, psem_opt );
    % time(3) = toc;
    %
    % tic
    % PSAEM_res = PSAEM_JMLGSS( sys, init, psaem_opt );
    % time(2) = toc;
    
    tic
    RB_PSAEM_res = RB_PSAEM_JMLGSS( sys, init, rb_psaem_opt );
    time(1) = toc;
    
    
    
    
    
    %%
    KLR_Mu = 5;
    
    % Prior
    mu0 = sys.z0;
    P0 = sys.P0;
    lnw0 = log(1);
    [~,z,lnw,mu,P] = GM_to_HGM(sys.K,lnw0,mu0,P0);
    prior.z = z;
    prior.lnw = lnw;
    prior.mu = mu;
    prior.P = P;
    
    lnL_RB_PSAEM = zeros(num_iter+1,1);
    for i = 1:num_iter+1
        param.lnT = RB_PSAEM_res.lnThist(:,:,i);
        param.A=RB_PSAEM_res.Ahist(:,:,:,i);
        param.B=RB_PSAEM_res.Bhist(:,:,:,i);
        param.C=RB_PSAEM_res.Chist(:,:,:,i);
        param.Q=RB_PSAEM_res.Qhist(:,:,:,i);
        param.R=RB_PSAEM_res.Rhist(:,:,:,i);
        tic
        lnL_RB_PSAEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
        toc
    end
    
    %
    % lnL_PSAEM = zeros(num_iter+1,1);
    % for i = 1:num_iter+1
    %     param.lnT = PSAEM_res.lnThist(:,:,i);
    %     param.A=PSAEM_res.Ahist(:,:,:,i);
    %     param.B=PSAEM_res.Bhist(:,:,:,i);
    %     param.C=PSAEM_res.Chist(:,:,:,i);
    %     param.Q=PSAEM_res.Qhist(:,:,:,i);
    %     param.R=PSAEM_res.Rhist(:,:,:,i);
    %     lnL_PSAEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
    % end
    %
    %
    % lnL_PSEM = zeros(num_iter+1,1);
    % for i = 1:num_iter+1
    %     param.lnT = PSEM_res.lnThist(:,:,i);
    %     param.A=PSEM_res.Ahist(:,:,:,i);
    %     param.B=PSEM_res.Bhist(:,:,:,i);
    %     param.C=PSEM_res.Chist(:,:,:,i);
    %     param.Q=PSEM_res.Qhist(:,:,:,i);
    %     param.R=PSEM_res.Rhist(:,:,:,i);
    %     lnL_PSEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
    % end
    
    
    
    lnL_JMLSEM = zeros(num_iter+1,1);
    for i = 1:num_iter+1
        param.lnT = JMLSEM_res.lnThist(:,:,i);
        param.A=JMLSEM_res.Ahist(:,:,:,i);
        param.B=JMLSEM_res.Bhist(:,:,:,i);
        param.C=JMLSEM_res.Chist(:,:,:,i);
        param.Q=JMLSEM_res.Qhist(:,:,:,i);
        param.R=JMLSEM_res.Rhist(:,:,:,i);
        priori = JMLSEM_res.prior_hist.step(i);
        
        numL = size(priori.cP,3);
        priori.P = nan(sys.nz,sys.nz,numL);
        for l=1:numL
            priori.P(:,:,l) = priori.cP(:,:,l).'*priori.cP(:,:,l);
        end
        
        lnL_JMLSEM(i) = Ln_Likelihood_of_soln( sys, param, priori, KLR_Mu);
    end
    
    
    
    
    
    %%
    figure(1);hold on;
    plot_5(time(1)/num_iter*(0:num_iter).',lnL_RB_PSAEM,2);
    hold on;
    % plot_5(time(2)/num_iter*(0:num_iter).',lnL_PSAEM,3);
    % plot_5(time(3)/num_iter*(0:num_iter).',lnL_PSEM,4);
    plot_5(time(4)/num_iter*(0:num_iter).',lnL_JMLSEM,5);
    
    if inumruns == 1 %,'PSAEM','PSEM'
        %legend('RB-PSAEM','Proposed EM','Location','best');
        xlabel('Computational time [s]');
        ylabel('ln(p(y))');
        title('Log-likelihood vs. Computational time')
        
        
    end
    
    drawnow;
    arx_lnL_RB_PSAEM(:,inumruns) = lnL_RB_PSAEM;
    % arx_lnL_PSAEM(:,inumruns) = lnL_PSAEM;
    % arx_lnL_PSEM(:,inumruns) = lnL_PSEM;
    arx_lnL_JMLSEM(:,inumruns) = lnL_JMLSEM;
    
end
%%

% $$$ linethick = 2;
% $$$ 
% $$$ figure(2);clf;
% $$$ timerr = time(4)/num_iter*(0:num_iter);
% $$$ 
% $$$ s = ':';
% $$$ RGB =['f0'; '02'; '7f'];% magenta
% $$$ RGBi = hex2dec(RGB)/255;
% $$$ w=4;
% $$$ %         plot(x,y,s,'Color',RGBi,'LineWidth',w);
% $$$ 
% $$$ sec_errhandles1 = shadedErrorBar(timerr, arx_lnL_JMLSEM.', {@mean,@std3}, 'lineprops', {s,'Color',RGBi,'LineWidth',w});
% $$$ hold on;
% $$$ timerr = time(1)/num_iter*(0:num_iter);
% $$$ 
% $$$ s = '-o';
% $$$ RGB =['fd'; 'c0'; '86'];%orange
% $$$ w = 4;
% $$$ RGBi = hex2dec(RGB)/255;
% $$$ 
% $$$ sec_errhandles2 = shadedErrorBar(timerr, arx_lnL_RB_PSAEM.', {@mean,@std3}, 'lineprops', {s,'Color',RGBi,'LineWidth',w});
% $$$ 
% $$$ % Bring mean and edge lines to top
% $$$ 
% $$$ 
% $$$ uistack(sec_errhandles2.mainLine,'top');
% $$$ uistack(sec_errhandles2.edge,'top');
% $$$ 
% $$$ uistack(sec_errhandles1.mainLine,'top');
% $$$ uistack(sec_errhandles1.edge,'top');
% $$$ 
% $$$ legend([sec_errhandles1.mainLine, sec_errhandles1.patch,sec_errhandles2.mainLine, sec_errhandles2.patch], 'Proposed method mean likelihood','Proposed method 3 SD confidence interval','RB-PSAEM mean likelihood','RB-PSAEM 3 SD confidence interval');
% $$$ ylabel('ln(p(y))')
% $$$ xlabel('Computational time [s]')
% $$$ title('Log-likelihood vs. Computational time')
%%
%save longrun

save(['long_run_' num2str(number) '_' datestr(now,'yyyymmddHHMMSS')])

%set(gca, 'XScale', 'log');
end