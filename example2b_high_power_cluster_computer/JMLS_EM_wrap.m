function res = JMLS_EM_wrap( sys, init, opt)


KLR_Mu = opt.KLR_Mu;
KLR_epsilon = opt.KLR_epsilon;
num_iter = opt.num_iter; 

% Input and measurement vectors
u_arr = [0 sys.U(1:end-1)]; %  Check this is compatible
y_arr = sys.Y;

% Sizes
nx = sys.nz;
nu = sys.nu; 
ny = sys.ny;
nz = sys.K; 

% Prior
mu0 = sys.z0;
P0 = sys.P0;
lnw0 = log(1);
[~,z,lnw,mu,P] = GM_to_HGM(nz,lnw0,mu0,P0);
prior.z = z;
prior.lnw = lnw;
prior.mu = mu;
prior.P = P;


Ahist = zeros(nx,nx,nz,num_iter+1); 
Bhist = zeros(nx,nu,nz,num_iter+1);
Chist = zeros(ny,nx,nz,num_iter+1); 
Qhist = zeros(nx,nx,nz,num_iter+1);
Rhist = zeros(ny,ny,nz,num_iter+1); 

A = init.A; 
B = init.B; 
C = init.C; 
Q = init.Q; 
R = init.R; 

Ahist(:,:,:,1) = A; 
Bhist(:,:,:,1) = B;
Chist(:,:,:,1) = C; 
Qhist(:,:,:,1) = Q; 
Rhist(:,:,:,1) = R;

for m = 1:nz
    JMLSm.A = A(:,:,m);
    JMLSm.B = B(:,:,m);
    JMLSm.C = C(:,:,m);
%     JMLSm.D = D(:,:,m);
    
    JMLSm.Q = Q(:,:,m);
    JMLSm.R = R(:,:,m);
    
    JMLS.model(m) = JMLSm;
end
JMLS.lnT = log(init.PI.');
JMLS.N = nz;

lnThist = zeros(nz,nz,num_iter+1); 
lnThist(:,:,1) = JMLS.lnT;

prior_hist.step(1) = prior;
lnL = nan;
for i =1:num_iter
    old_lnL = lnL;
%     old_lnT = JMLS.lnT;
    
%     tic;
    [JMLS,prior,lnL]  = JMLS_EM_NO_D(prior,u_arr,y_arr,JMLS,KLR_Mu, KLR_epsilon);
%     toc
    
    lnThist(:,:,i+1) = JMLS.lnT;
    
%     if i < 50
%         JMLS.lnT = old_lnT;
%     end
    
    for m=1:nz
        mmodel = JMLS.model(m);

        Ahist(:,:,m,i+1) = mmodel.A; 
        Bhist(:,:,m,i+1) = mmodel.B;
        Chist(:,:,m,i+1) = mmodel.C; 
        
        Qhist(:,:,m,i+1) = mmodel.Q; 
        Rhist(:,:,m,i+1) = mmodel.R;
    end
    prior_hist.step(i+1) = prior;
    
    if i>1
        display(['JMLS EM iteration ',num2str(i),' completed. Increase in LL: ', num2str(lnL-old_lnL)]);
    else
        display(['JMLS EM iteration ',num2str(i) ' completed.']);
    end
    
end


res.Ahist = Ahist;
res.Bhist = Bhist;
res.Chist = Chist;
res.Qhist = Qhist;
res.Rhist = Rhist;
res.lnThist = lnThist;
res.prior_hist = prior_hist;

disp('Done!');

