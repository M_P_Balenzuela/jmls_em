# README #

This is a repository contains supplementary MATLAB code for the paper titled:
Parameter Estimation for Jump Markov Linear Systems

By
Mark P. Balenzuela, Adrian G. Wills, Christopher Renton, and Brett Ninness.


### How do I run the code? ###
Code for the four examples is provided in the respective folders.
Call the run_me.m file in the relevant folder to run the example.

To run the code, the MATLAB v9.4 (2018a) or later must be installed with toolboxes:

	-'Control System Toolbox v10.4 or later'
	
	-'Statistics and Machine Learning Toolbox v11.3 or later'



### Who do I talk to? ###
Please contact mark.balenzuela@uon.edu.au for all inquiries.