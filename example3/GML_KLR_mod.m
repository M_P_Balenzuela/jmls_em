function [M_red,L_red,s_red,r_red] = GML_KLR(L,s,r, epsilon,minPerCompatible)

        
    [nx,~,M] = size(L);

    L_red = nan(nx,nx,M);
    s_red = nan(nx,M);
    r_red = nan(M,1);
    
    M_red = 0;

    while M >0
        Li = L(:,:,1);
        si = s(:,1);
        ri = r(1);

        compIdx = false(M,1);
        compIdx(1) = true;
        for j = 2:M
            Lj = L(:,:,j);
            sj = s(:,j);
            rj = r(j);

            [~,tmp] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon);

            if ~isinf(tmp)
                compIdx(j) = true; 
            end    
        end

        [Mr, Lr,sr,rr] = KLR_reduce_compatible_likelihoods(L(:,:,compIdx),s(:,compIdx),r(compIdx), epsilon,minPerCompatible);
        
        idx = (M_red+1):(M_red+Mr);
        M_red= M_red + Mr;
        L_red(:,:,idx) = Lr;
        s_red(:,idx)= sr;
        r_red(idx) = rr;

        L = L(:,:,~compIdx);
        s = s(:,~compIdx);
        r = r(~compIdx);

        M = M-sum(real(compIdx));

    end

    idx = 1:M_red;
    L_red = L_red(:,:,idx);
    s_red = s_red(:,idx);
    r_red = r_red(idx);
    

end



function [M,L,s,r] = KLR_reduce_compatible_likelihoods(L,s,r, epsilon, minPerCompatible)

    M = size(L,3);
    while M > minPerCompatible
        [i,j] = find_merge_pair(L,s,r,epsilon);
        Li = L(:,:,i);
        si = s(:,i);
        ri = r(i);
        Lj = L(:,:,j);
        sj = s(:,j);
        rj = r(j);    

        [~,~,Lij,sij,rij] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon);
        
        L(:,:,i) = Lij;
        s(:,i) = sij;
        r(i) = rij;
        
        ej = true(M,1);
        ej(j) = false;
        L = L(:,:,ej);
        s = s(:,ej);
        r = r(ej);
        
        M = M-1;
    end

end


function [i,j] = find_merge_pair(L,s,r,epsilon)

    M = size(L,3);

    relB = inf(M,M);
    convto1 = ones(M,1);
    % only fill the UT of relB
    for j = 2:M
        Lj = L(:,:,j);
        sj = s(:,j);
        rj = r(j);
        for i = 1:j-1

            Li = L(:,:,i);
            si = s(:,i);
            ri = r(i);

            [wjmw1,relBij] = GL_merge(Li,si,ri,Lj,sj,rj, epsilon);
            relB(i,j) = relBij;
            if i == 1
                convto1(j) = wjmw1; %wj-w1
            end
        end
    end

    relB = repmat(convto1,1,M) .* relB;

    [i,j] = minMat(relB);
    
    

end