function [cLb,sb,rb] = N_cBIF_BackPropagation(cLc,sc,rc,A,b,cQ)

persistent struct_UT_TRANSA struct_UT 
if isempty(struct_UT_TRANSA) 
    struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   % is the argument UT, does it need to be transposed?
    struct_UT = struct('UT',true);
end

nx = size(A,1);

cinvQ = linsolve(cQ,eye(nx),struct_UT_TRANSA); % Inverse of UT matrix
tmp = linsolve(cQ,A,struct_UT_TRANSA);

QR2 = [cLc zeros(nx);
    cinvQ tmp];

% Q-less QR decomp.

R2 = triu(qr(QR2));
cLb = R2(nx+1:end,nx+1:end); % R1_22

R2_11 = R2(1:nx,1:nx);
R2_12 = R2(1:nx,nx+1:end);


Cy = linsolve(R2_11,R2_12,struct_UT); % Cy = A.'/(cQ.'*cQ)*O
Cy = Cy.'; 

Cx = linsolve(cQ,b,struct_UT_TRANSA);
rb = Cx.'*Cx;
Cx = linsolve(cQ,Cx,struct_UT); %Cx = invQ*b

tmp = sc - Cx; %(s - (cQ.'*cQ)\b)
sb =  Cy*tmp;
sb = sb + A.'*Cx;

if nargout > 2
    NCs = TrLn(cQ) + TrLn(R2_11);
    NCs = 2*NCs;
    rb = rb + NCs;
    
    
    tmp = linsolve(R2_11,tmp,struct_UT_TRANSA);
    rb = rb - tmp.'*tmp;
    
    rb = rc + rb;
end