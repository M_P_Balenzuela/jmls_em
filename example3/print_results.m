function print_results(true_sys, EM_res_output,map)
nz = size(true_sys.A,3);
%bar = true
[ALSQ,bLSQ] = LSQ_of_modeli(true_sys, EM_res_output, 1);
for i = 2:nz
    [ALSQi,bLSQi] = LSQ_of_modeli(true_sys, EM_res_output, i);
    ALSQ = [ALSQ;ALSQi];
    bLSQ = [bLSQ;bLSQi];
end

    %overconstrained, but gives lsqr soln.
    Transformation = ALSQ\bLSQ;
    
    %map = find_map(true_sys, EM_res_output);
    
    Tsyst = transformed_system(EM_res_output,Transformation,map);
    
    for i = 1:nz
    disp('-------------------------------------------------------')
    disp(['Model ' num2str(i)]);
    disp('-------------------------------------------------------')
    disp(['True A(' num2str(i) '): ' num2str(true_sys.A(:,:,i)) ', found A(' num2str(i) '): ' num2str(num2str(Tsyst.A(:,:,i))) ]);
    disp(['True B(' num2str(i) '): ' num2str(true_sys.B(:,:,i)) ', found B(' num2str(i) '): ' num2str(num2str(Tsyst.B(:,:,i))) ]);
    disp(['True C(' num2str(i) '): ' num2str(true_sys.C(:,:,i)) ', found C(' num2str(i) '): ' num2str(num2str(Tsyst.C(:,:,i))) ]);
    disp(['True Q(' num2str(i) '): ' num2str(true_sys.Q(:,:,i)) ', found Q(' num2str(i) '): ' num2str(num2str(Tsyst.Q(:,:,i))) ]);
    disp(['True R(' num2str(i) '): ' num2str(true_sys.R(:,:,i)) ', found R(' num2str(i) '): ' num2str(num2str(Tsyst.R(:,:,i))) ]);
    end
    disp('-------------------------------------------------------')
    
for j=1:nz
    for i = 1:nz
        disp(['True T(' num2str(i) ',' num2str(j) '): ' num2str(true_sys.PI(j,i)) ', Found T(' num2str(i) ',' num2str(j) '): ' num2str(Tsyst.Pi(j,i))]);
    end
end

disp('-------------------------------------------------------')
disp('-------------------------------------------------------')
end

function Tsyst = transformed_system(EM_res_output,Tran,map)
    nz = size(EM_res_output.Ahist,3);
    for i = 1:nz
        Tsyst.A(:,:,i) = EM_res_output.Ahist(:,:,map(i),end);
        Tsyst.B(:,:,i) = Tran*EM_res_output.Bhist(:,:,map(i),end);
        Tsyst.C(:,:,i) = Tran\EM_res_output.Chist(:,:,map(i),end);
%         Tsyst.D(:,:,i) = EM_res_output.Dhist(:,:,map(i),end);
        Tsyst.Q(:,:,i) = Tran^2*EM_res_output.Qhist(:,:,map(i),end);
        Tsyst.R(:,:,i) = EM_res_output.Rhist(:,:,map(i),end);
%         Tsyst.S(:,:,i) = Tran*EM_res_output.Shist(:,:,map(i),end);
    end
    
    Tsyst.Pi = nan(nz);
    for i =1:nz
        for j = 1:nz
            Tsyst.Pi(i,j) = exp(EM_res_output.lnThist(map(j),map(i),end));
        end
    end
end

function map = find_map(true_sys, EM_res_output)

nz = size(true_sys.A,3);

Am = nan(1,nz);
Ama = nan(1,nz); %actual = true
for i = 1:nz
    Ama(i) = true_sys.A(:,:,i);
    Am(i) = EM_res_output.Ahist(:,:,i,end);
%     Ama(i) = true_sys.PI(i,i);
%     Am(i) = exp(EM_res_output.lnThist(i,i,end));
end

Mc = abs(Am.'-Ama);
IDXc = perms(1:nz); % map j=IDXc(i), where i=actual idx, j = JMLS idx
nIDXc = size(IDXc,1);

Tc = nan(1,nIDXc);
for i = 1:nIDXc
    Tc(i) = sum(Mc(IDXc(i,:)+(0:nz-1)*nz));
end
[~, mini] = min(Tc);
map = IDXc(mini,:);

end

function [ALSQi,bLSQi] = LSQ_of_modeli(true_sys, EM_res_output, i)
%bar = true

ALSQi = [EM_res_output.Bhist(:,:,i,end);
        true_sys.C(:,:,i);
        sqrt(EM_res_output.Qhist(:,:,i,end))];
    
bLSQi = [ true_sys.B(:,:,i);   
        EM_res_output.Chist(:,:,i,end);
        sqrt(true_sys.Q(:,:,i));];
end