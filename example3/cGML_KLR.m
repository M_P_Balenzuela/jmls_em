function [M_red,cL_red,s_red,r_red] =  cGML_KLR(cL_arr,s_arr,r_arr,epsilon,minPerCompatible)


    [nx,~,M] = size(cL_arr);

    rankcL = nan(M,1);
    RM = nan(nx,nx,M);
    QM = nan(nx,nx,M);

    for i = 1:M % over all pairs, not just compatible ones
        [QMi,RMi] = qr(cL_arr(:,:,i).');
        rankcL(i) = sum(diag(abs(RMi)) > epsilon);
        RM(:,:,i) = triu(RMi);
        QM(:,:,i) = QMi;
    end


    cL_red = nan(nx,nx,M);
    s_red = nan(nx,M);
    r_red = nan(M,1);

    M_red = 0;

    activeIdx = true(M,1);

    for i=1:M
        ni = rankcL(i);
        if activeIdx(i)
            compIdx = false(M,1);
            compIdx(i) = true;
            for j = i+1:M
                if activeIdx(j)
                    compIdx(j) = modes_compatible(ni,rankcL(j),QM(:,:,i),QM(:,:,j),epsilon); 
                end
            end

            [Mr, cLr,sr,rr] = cKLR_reduce_compatible_likelihoods(ni,nx,cL_arr(:,:,compIdx),s_arr(:,compIdx),r_arr(compIdx),RM(1:ni,1:ni,compIdx),QM(:,1:ni,compIdx),minPerCompatible);

            activeIdx = activeIdx & (~compIdx);

            idx = (M_red+1):(M_red+Mr);
            M_red = M_red + Mr;
            cL_red(:,:,idx) = cLr;
            s_red(:,idx)= sr;
            r_red(idx) = rr;

        end

    end
    
    cL_red = cL_red(:,:,1:M_red);
    s_red = s_red(:,1:M_red);
    r_red = r_red(1:M_red);

end
    
    
    
function ret = modes_compatible(ni,nj,QMi,QMj,epsilon)
        
    % check for compatible range spaces
    if (ni ~= nj) % range spaces can't possibly be equal with different rank
        ret = false;
        return;
    end

    Vi = QMi(:,1:ni);
    Zi = QMi(:,ni+1:end);

    Vj = QMj(:,1:nj);
    Zj = QMj(:,nj+1:end);

    % Check null and range space basis have no overlap (dot product zero)
    Ck1 = Vi.'*Zj;
    Ck2 = Vj.'*Zi;

    if any(abs(Ck1(:)) > epsilon) || any(abs(Ck2(:)) > epsilon)
        ret = false;
    else
        ret = true;
    end
    return;

end



function [Mr, cLr_arr,sr_arr,rr_arr] = cKLR_reduce_compatible_likelihoods(ni,nx,cL_arr,s_arr,r_arr,SigmaT,V,minPerCompatible)

    M = size(r_arr,1);
    Mr = M;
    activeIdx = true(M,1);
    
    % Allocate space
    e = nan(M,1);
    mu = nan(ni,M);
    invSigma = nan(ni,ni,M);
    logdetSigma = nan(M,1);
    
    % Pre-compute commonly used stats
    for i =1:M
        [e(i),mu(:,i),invSigma(:,:,i),logdetSigma(i)] = compute_indep_stats(ni,SigmaT(:,:,i),V(:,:,i),s_arr(:,i),r_arr(i));
    end
    
    % Reorder based off e(i) here. This is completed for numberical
    % stability as the algorithm generates exp(ej-ei) and it's best for
    % ei>ej to avoid infs.
    [~,I] = sort(e,'descend');
    e = e(I);
    mu = mu(:,I);
    invSigma = invSigma(:,:,I);
    logdetSigma = logdetSigma(I);
    V = V(:,:,I);
    cL_arr = cL_arr(:,:,I);
    s_arr = s_arr(:,I);
    r_arr = r_arr(I);
    
    % Allocate space
    ewjmwi = nan(M,M);
    Bij_reli = inf(M,M);
    logdetSigmaij = nan(M,M);
    invSigmaij_half = nan(ni,ni,M,M);
    vi = nan(M,M);
    muj = nan(ni,M,M);    
    
    
    % precompute joint stats
    for j =1:M
        for i = 1:j-1
            [ewjmwi(i,j),Bij_reli(i,j),logdetSigmaij(i,j),invSigmaij_half(:,:,i,j),vi(i,j),muj(:,i,j)] = compute_joint_stats(ni,mu(:,i),invSigma(:,:,i),V(:,:,i),logdetSigma(i),cL_arr(:,:,j),s_arr(:,j),e(i),e(j));
        end
        ewjmwi(j,j) = 1;
    end
    
    while Mr > minPerCompatible
        newnumba1 = find(activeIdx,1,'first');
        Bmat = inf(M,M);
        
        Bmat(activeIdx,activeIdx) = ewjmwi(newnumba1,activeIdx).' .*  Bij_reli(activeIdx,activeIdx);

        [i,j] = minMat(Bmat);

        % Merge pair i-j
        [cLij,sij,rij] = merge_pair(ni,nx,logdetSigmaij(i,j),invSigmaij_half(:,:,i,j),V(:,:,i),vi(i,j),mu(:,i),muj(:,i,j),e(i),e(j));
        % new mode replaces mode i as i<j
        cL_arr(:,:,i) = cLij;
        s_arr(:,i) = sij;
        r_arr(i) = rij;
        activeIdx(j) = false;

        Mr = Mr-1;
        if Mr == minPerCompatible
            break;
        end

        % Update stats for mode i
        [QMi,RMi] = qr(cL_arr(:,:,i).');
        RMi = triu(RMi);
        SigmaT(:,:,i) = RMi(1:ni,1:ni);
        V(:,:,i) = QMi(:,1:ni);
        [e(i),mu(:,i),invSigma(:,:,i),logdetSigma(i)] = compute_indep_stats(ni,SigmaT(:,:,i),V(:,:,i),s_arr(:,i),r_arr(i));

        % Update joints stats 
            for ii = 1:i-1
                if activeIdx(ii)
                    [ewjmwi(ii,i),Bij_reli(ii,i),logdetSigmaij(ii,i),invSigmaij_half(:,:,ii,i),vi(ii,i),muj(:,ii,i)] = compute_joint_stats(ni,mu(:,ii),invSigma(:,:,ii),V(:,:,ii),logdetSigma(ii),cL_arr(:,:,i),s_arr(:,i),e(ii),e(i));
                end
            end
        
            for jj = i+1:M
                if activeIdx(jj)
                    [ewjmwi(i,jj),Bij_reli(i,jj),logdetSigmaij(i,jj),invSigmaij_half(:,:,i,jj),vi(i,jj),muj(:,i,jj)] = compute_joint_stats(ni,mu(:,i),invSigma(:,:,i),V(:,:,i),logdetSigma(i),cL_arr(:,:,jj),s_arr(:,jj),e(i),e(jj));
                end
            end
        
    end
    

    
    cLr_arr = cL_arr(:,:,activeIdx);
    sr_arr = s_arr(:,activeIdx);
    rr_arr = r_arr(activeIdx);

end


function [ei,mui,invSigmai,logdetSigmai] = compute_indep_stats(ni,SigmaiT,Vi,si,ri)

    persistent struct_UT_TRANSA struct_UT 
    if isempty(struct_UT_TRANSA) 
        struct_UT_TRANSA = struct('UT',true,'TRANSA',true);  
        struct_UT = struct('UT',true);
    end

    etai = Vi.'*si; 

    logdetSigmai = TrLn(SigmaiT);

    tmpai = linsolve(SigmaiT,etai,struct_UT); 

    ei = -0.5*(ri-tmpai.'*tmpai) - logdetSigmai;

    mui = linsolve(SigmaiT,tmpai,struct_UT_TRANSA);

    invSigmai = linsolve(SigmaiT,eye(ni),struct_UT);
    
end


function [ewjmwi,Bij_reli,logdetSigmaij,invSigmaij_half,vi,muj] = compute_joint_stats(ni,mui,invSigmai,Vi,logdetSigmai,cLj,sj,ei, ej)

    persistent struct_UT_TRANSA struct_UT 
    if isempty(struct_UT_TRANSA) 
        struct_UT_TRANSA = struct('UT',true,'TRANSA',true);  
        struct_UT = struct('UT',true);
    end
    

    Sigmaj = cLj*Vi; % we already checked ni=nj
    Sigmaj = triu(qr(Sigmaj));
    Sigmaj = Sigmaj(1:ni,1:ni);
    % now sigmaj is square, and UT


    etaj = Vi.'*sj;
    tmpaj = linsolve(Sigmaj,etaj,struct_UT_TRANSA); 

    logdetSigmaj = TrLn(Sigmaj);


    eij = lnw_normalise([ei ej]);
    ein = eij(1);
    ejn = eij(2);

    ewjmwi = exp(ej-ei);
    
    vi = exp(ein); 
    vj = exp(ejn); 
    
    sqrt_vi = sqrt(vi);
    sqrt_vj = sqrt(vj);
    

    muj = linsolve(Sigmaj,tmpaj,struct_UT);
    invSigmaj = linsolve(Sigmaj,eye(ni),struct_UT_TRANSA);

    % Begin merging
    QR1 =[sqrt_vi*invSigmai; sqrt_vj*invSigmaj; sqrt_vi*sqrt_vj*(mui-muj).'];
    invSigmaij_half = triu(qr(QR1));
    
    
    invSigmaij_half = invSigmaij_half(1:ni,1:ni);
    logdetSigmaij = TrLn(invSigmaij_half);
    Bij_reli = logdetSigmai + logdetSigmaij + ewjmwi*(logdetSigmaj + logdetSigmaij);
    
end

function [cLij,sij,rij] = merge_pair(ni,nx,logdetSigmaij,invSigmaij_half,Vi,vi,mui,muj,ei,ej)

    persistent struct_UT_TRANSA 
    if isempty(struct_UT_TRANSA) 
        struct_UT_TRANSA = struct('UT',true,'TRANSA',true);  
    end

    vj = 1 - vi;
    tmpp = linsolve(invSigmaij_half, vi*mui+vj*muj, struct_UT_TRANSA);

    rij = logdetSigmaij;
    rij = rij - LSE([ei;ej]);
    rij = 2*rij;
    rij = rij + tmpp.'*tmpp;

    tmppres = linsolve(invSigmaij_half, Vi.', struct_UT_TRANSA);
    sij = tmppres.'*tmpp;

    cLij = triu(qr(tmppres));
    cLij = [cLij;zeros(nx-ni,nx)];      
    
end
