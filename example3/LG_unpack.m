function [A,b,Q,C,d,R] = LG_unpack(SS,u)
% Unpack a Linear Gaussian SS structure. This function also transforms the system to remove
% cross-covariance noise S.

% IN:
    % SS.A - A in x_{k+1} = Ax_k + Bu
    % SS.C - C in y = Cx + Du
    % SS.Q - Process noise covariance matrix
    % SS.R - Measurement noise covariance matrix
    
% OPTIONAL IN:
    % SS.B - B in x_{k+1} = Ax_k + Bu
    % SS.D - D in y = Cx + Du
    % ym - Measurement vector (required if SS.S exists)
    % u - Input vector
    
% OUT:
    % A - A in x_{k+1} = Ax_k + b
    % b - State prediction offset
    % Q - New process noise covariance matrix
    % C - C in y= Cx + d
    % d - Measurement prediction offset
    % R - Measurement noise covariance matrix
    


if isfield(SS,'S')
	error('The LG system class cannot have cross-covariance S, perhaps you are after a dynLG class?');
end


if nargin < 2
    u = zeros(0,1);
end
    
% Unpack required variables
A = SS.A;
C = SS.C;
Q = SS.Q;
R = SS.R;

% Grab dims.
[nu,~] = size(u);
[ny,nx] = size(C);

if isfield(SS,'B')
    B = SS.B;
else
    B = zeros(nx,nu);
end

if isfield(SS,'D')
    D = SS.D;
else
    D = zeros(ny,nu);
end


u_pred = u;

b = B*u_pred;
d = D*u;