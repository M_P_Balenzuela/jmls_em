function [A,b,cQ,C,d,cR] = cLG_unpack(SS,u)



% Unpack a Linear Gaussian SS structure. This function also transforms the system to remove
% cross-covariance noise S.

% IN:
    % SS.A - A in x_{k+1} = Ax_k + Bu
    % SS.C - C in y = Cx + Du
    % SS.Q - Process noise covariance matrix
    % SS.R - Measurement noise covariance matrix
    
% OPTIONAL IN:
    % SS.B - B in x_{k+1} = Ax_k + Bu
    % SS.D - D in y = Cx + Du
    % SS.S - Cross-covaraince between process and measurement noise
    % ym - Measurement vector (required if SS.S exists)
    % u - Input vector
    
% OUT:
    % A - A in x_{k+1} = Ax_k + b
    % b - State prediction offset
    % Q - New process noise covariance matrix
    % C - C in y= Cx + d
    % d - Measurement prediction offset
    % R - Measurement noise covariance matrix
    


if nargin < 2
    u = zeros(0,1);
end
    
% Unpack required variables
A = SS.A;
C = SS.C;
cQ = SS.cQ;
cR = SS.cR;

% Grab dims.
[nu,~] = size(u);
[ny,nx] = size(C);

if isfield(SS,'B')
    B = SS.B;
else
    B = zeros(nx,nu);
end

if isfield(SS,'D')
    D = SS.D;
else
    D = zeros(ny,nu);
end

% if isfield(SS,'S') % not sqrt S
% 	error('The LG system class cannot have cross-covariance S, perhaps you are after a dynLG class?');
% end

    
b = B*u;
d = D*u;