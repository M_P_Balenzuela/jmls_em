function [cJMLS,prior,lnL]  = cJMLS_EM_NO_D(prior,u_arr,y_arr,cJMLS, KLR_Mu, KLR_epsilon)
% Perform one itteration of the EM algorithm for a chol- JMLS


persistent struct_UT 
if isempty(struct_UT) 
    struct_UT = struct('UT',true);
end


% E-Step
 [~, JMLSS, lnL] = cJMLS_jointTFS_EstBatch(cJMLS, u_arr, y_arr, prior, KLR_Mu, KLR_epsilon);
 

%% M-Step


nz = cJMLS.N; 
nx = size(prior.mu,1);
nu = size(u_arr,1);
ny = size(y_arr,1);
N = length(JMLSS.dist);

two_nx = 2*nx;
nE = two_nx+ny+nu;
LargeExpectation = zeros(nE,nE,nz);

lnTk = -inf*ones(nz);

for k = 1:N

    M = size(JMLSS.dist(k).mu,2);
    ym = y_arr(:,k+1);
    u = u_arr(:,k+1);
        
    lnw_arr = JMLSS.dist(k).lnw;
    mu_arr = JMLSS.dist(k).mu;
    cP_arr = JMLSS.dist(k).cP;
    z_arr = JMLSS.dist(k).z;
        
    for l=1:M
%         zl = z_arr(1,l); 
        z2 = z_arr(2,l);
        
        lnwl = lnw_arr(l);
        mul = mu_arr(:,l);
        cPl = cP_arr(:,:,l);
        
        MEE = cEM_joint_expectation(cPl, mul, lnwl, u, ym);
        
        QR1 = [LargeExpectation(:,:,z2); MEE];
        tmp = triu(qr(QR1));
        LargeExpectation(:,:,z2) = tmp(1:nE,1:nE);

    end
    
    for i = 1:nz
        for j = 1:nz
            idxx = sum((z_arr==[j;i]),1)==2;
            idxx = idxx.';
            
            tmpa = [lnTk(i,j);lnw_arr(idxx)];
            lnTk(i,j) = LSE(tmpa);
        end
    end
									
end



cJMLS.lnT = lnw_normalise(lnTk); % protect against inpossible models here and in noramlise_lnw function

Cme = LSE(lnTk.');
Scale = -0.5*Cme;
Scale = exp(Scale);


TTrans_sqR = [zeros(nx,ny) zeros(nx);
             zeros(nx,ny) eye(nx);
             zeros(nu,ny) zeros(nu,nx);
             eye(ny) zeros(ny,nx)];
         
TTrans_sqQ = [zeros(nx) eye(nx) zeros(nx,nu);
    eye(nx) zeros(nx) zeros(nx,nu);
    zeros(nu,nx) zeros(nu,nx) eye(nu);
    zeros(ny,nx) zeros(ny,nx) zeros(ny,nu)];
       
for m = 1:nz
    
        % if model was active ...
        if (Cme(m) ~= -inf)
            
            tmp_sq = LargeExpectation(:,:,m).'*LargeExpectation(:,:,m);
            idxs = [1:nx 2*nx+1:2*nx+nu];
            Ezeta_eta = tmp_sq(nx+1:2*nx,idxs);
            Eeta_eta = tmp_sq(idxs,idxs);
            Gamma_P = Ezeta_eta/Eeta_eta;
            cJMLS.model(m).A = Gamma_P(:,1:nx);
            cJMLS.model(m).B = Gamma_P(:,nx+1:end); 
            
            QR2 = LargeExpectation(:,:,m) * TTrans_sqQ *[-eye(nx); Gamma_P.'];
            tmp = triu(qr(QR2));
            cJMLS.model(m).cQ = Scale(m)*tmp(1:nx,1:nx);
            

            C = tmp_sq(2*nx+nu+1:end,nx+1:2*nx)/tmp_sq(nx+1:2*nx,nx+1:2*nx);
            cJMLS.model(m).C = C;
            cJMLS.model(m).D = zeros(ny,nu);
            
            QR3 = LargeExpectation(:,:,m) * TTrans_sqR *[-eye(ny); C.'];
            tmp2 = triu(qr(QR3));
            cJMLS.model(m).cR = Scale(m)*tmp2(1:ny,1:ny);            
            

        end
end


% Generate new prior
prior.mu = JMLSS.dist(1).mu(1:nx,:);
prior.cP = JMLSS.dist(1).cP(1:nx,1:nx,:);
prior.z = JMLSS.dist(1).z(1,:).';
prior.lnw = JMLSS.dist(1).lnw;

[prior.z,prior.lnw, prior.mu, prior.cP] = cHGM_KLR(prior.z,prior.lnw, prior.mu, prior.cP, KLR_Mu);


