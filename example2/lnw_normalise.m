function lnwn = lnw_normalise(lnw)
% Vectorwise normalisation of log weights lnw, normalised along first
% dimension (each column)

% Using the log-sum-exp trick:
%https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/

% Find max value
% lnw = real(lnw); % for robustness, but hides numerical instability.
lnwmax = max(lnw);

% protection against columns being entirely -inf
idx = (lnwmax == -inf);
lnw(:,idx) = 0;
lnwmax(idx) = 0;

lnwn = lnw-lnwmax-log(sum(exp(lnw-lnwmax))); % derivation in thesis

