function [M_red,L_red,s_red,r_red] = GML_KLR_singleState(L_arr,s_arr,r_arr, epsilon,minPerCompatible)
[P_arr,mu_arr,lnw_arr] = IFM_to_GM(L_arr,s_arr,r_arr);

Ml=1;
lambda=0;
[M_red, lnw_arr, mu_arr, P_arr] = GM_KLR(lnw_arr, mu_arr, P_arr, minPerCompatible, Ml, lambda);

[L_red,s_red,r_red] = GM_to_IFM(P_arr, mu_arr, lnw_arr);
end