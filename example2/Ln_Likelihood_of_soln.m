function lnL = Ln_Likelihood_of_soln( sys, init, prior, KLR_Mu)


% Input and measurement vectors
u_arr = [0 sys.U(1:end-1)]; %  Check this is compatible
y_arr = sys.Y;


nz = sys.K; 



A = init.A; 
B = init.B; 
C = init.C; 
Q = init.Q; 
R = init.R; 



for m = 1:nz
    JMLSm.A = A(:,:,m);
    JMLSm.B = B(:,:,m);
    JMLSm.C = C(:,:,m);
%     JMLSm.D = D(:,:,m);
    
    JMLSm.Q = Q(:,:,m);
    JMLSm.R = R(:,:,m);
    
    JMLS.model(m) = JMLSm;
end
JMLS.lnT = init.lnT;
JMLS.N = nz;




% Grab dimensions
[~,N] = size(u_arr);

% Set prior
zf = prior.z;
lnwf = prior.lnw;
muf = prior.mu;
Pf = prior.P;



lnL = 0;
% Run the JMLS filter over the timeseries
for k = 1:N-1
    u = u_arr(:,k+1);
    ym = y_arr(:,k+1);

    % Run JMLS filter
    [lnwf,muf,Pf,zf, lnLk] = JMLS_filter(lnwf,muf,Pf,zf,JMLS,ym,u);
    
    lnL = lnL + lnLk;
    
    % KL Reduction
    [zf, lnwf, muf, Pf] = HGM_KLR(zf,lnwf, muf, Pf, KLR_Mu);


end