function [maghandle, phase_handle]= my_bode(sys)

[a,b,c] = bode(sys);
subplot(2,1,1);
maghandle = semilogx(c,20*log10((squeeze(a))));
title('Bode Diagram');
ylabel('Magnitude (dB)');
hold on
subplot(2,1,2);
phase_handle = semilogx(c,squeeze(b));
ylabel('Phase (deg)');
xlabel('Frequency (rad/s)');
hold on;