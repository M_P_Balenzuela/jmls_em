clear
 rng(1);
%%%%%%%%%%%%%%%%%%%
% Some parameters %
%%%%%%%%%%%%%%%%%%%
sys.K = 2;              % Number of modes
sys.nz = 2;             % State dimension
sys.nu = 1;             % Input dimension
sys.ny = 1;             % Output dimension
sys.T = 7000;%7           % Number of simulated data points
W = 0.4;                  % Relative disturbance of initial guesses
N1 = 25;               % Number of particles in RB-PSAEM
N2 = 118;                % Number of particles in PSAEM
N3 = 75; M3 = 34 ;      % Number of particles and backward trajectories in PSEM
num_iter =500%300%750      % Number of iterations
kappa = 1; b = 100;      % Tuning paramters for SA sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%

true_sys.A = zeros(sys.nz,sys.nz,sys.K); true_sys.C = zeros(sys.ny,sys.nz,sys.K);
true_sys.B = zeros(sys.nz,sys.nu,sys.K); true_sys.D = zeros(sys.ny,sys.nz,sys.K);
true_sys.Q = zeros(sys.nz,sys.nz,sys.K); true_sys.R = zeros(sys.ny,sys.ny,sys.K);
true_sys.PI = 0.9*eye(sys.K) + 0.05/sys.K*ones(sys.K);
true_sys.sys.z0 = 0; true_sys.sys.P0 = eye(sys.nz);
sys.z0 = zeros(sys.nz,1); sys.P0 = eye(sys.nz);

gamma = zeros(1,num_iter);gamma(1:2) = 1; gamma(3:b) = 0.95;
gamma(b+1:end) = 0.95*(((0:num_iter-b-1)+kappa)/kappa).^(-0.7);

rb_psaem_opt.N = N1; rb_psaem_opt.gamma = gamma; rb_psaem_opt.num_iter = num_iter;
psaem_opt.N = N2; psaem_opt.gamma = gamma; psaem_opt.num_iter = num_iter;
psem_opt.N = N3; psem_opt.M = M3; psem_opt.num_iter = num_iter;

% Generate system and data
for n = 1:sys.K
    true_sys.A(:,:,n) = diag(2*rand(1,sys.nz)-1);
    true_sys.B(:,:,n) = 5*rand(sys.nz,sys.nu);
    true_sys.C(:,:,n) = 5*rand(sys.ny,sys.nz);
    true_sys.Q(:,:,n) = eye(sys.nz)*(0.01 + 0.09*rand);
    true_sys.R(:,:,n) = eye(sys.ny)*(0.01 + 0.09*rand);
    temp = rand(1,sys.K);
    true_sys.PI(n,:) = true_sys.PI(n,:) + 0.05*temp/sum(temp);
end

true_sys.PI = [0.6 0.5;
              0.4 0.5].';
%           
%   true_sys.PI = [0.6 0.3;
%                 0.4 0.7].';

true_sys.A(:,:,1) = [-0.3089  -0.5857;
     0.5857  -0.3089];
   
true_sys.B(:,:,1) = [-0.2033
                   -0.628];
 
true_sys.C(:,:,1) = [-0.8965   -0.889];

true_sys.A(:,:,2) = [0.604  -0.2859;
     -0.2859   0.5849];
true_sys.B(:,:,2) = [-2.288
     -0.8714];
true_sys.C(:,:,2) = [0  1.677];


true_sys.Q(:,:,1) = diag([0.045 0.03]);
true_sys.Q(:,:,2) = diag([0.032 0.02]);

true_sys.R(:,:,1) = 0.002;
true_sys.R(:,:,2) = 0.005;

sys.U = randn(sys.nu,sys.T); 

% usys = zpk([],[-0.01 -2 -3],1); % For generating a lowpass-filtered input signal sys.U
% for n = 1:sys.nu
%     temp = randn(1,sys.T); sys.U(n,:) = lsim(usys,temp,0:sys.T-1)';
% end

S = zeros(1,sys.T+1); Z = zeros(sys.nz,sys.T+1);
Z(:,1) = 0; sys.Y = zeros(sys.ny,sys.T); S(1) = 1;
for t = 1:sys.T
    S(t+1) = find(rand<=cumsum(true_sys.PI(S(t),:)),1);
    Z(:,t+1) = true_sys.A(:,:,S(t+1))*Z(:,t) + true_sys.B(:,:,S(t+1))*sys.U(:,t) + mvnrnd(zeros(sys.nz,1),true_sys.Q(:,:,S(t+1)))';
    sys.Y(:,t) = true_sys.C(:,:,S(t))*Z(:,t) + mvnrnd(zeros(sys.ny,1),true_sys.R(:,:,S(t)))';
end


init.A(:,:,1) = [-0.06993  -0.02433;
     -0.02433  -0.09378];

init.B(:,:,1) = [1.813
          -0.9379];

init.C(:,:,1) = [1.53  0];

init.A(:,:,2) = [0.5581  -0.1922;
     -0.1922   0.7987];

init.B(:,:,2) = [0.2615
               1.112];

init.C(:,:,2) = [-0.007845     -0.981 ];


init.Q(:,:,1) = diag([3.5 1.2]);
init.Q(:,:,2) = diag([1.3 2.0]);

init.R(:,:,1) = 0.05;
init.R(:,:,2) = 0.025;


init.PI = ones(sys.K)/sys.K;

JMLSEMopt.KLR_Mu = 3;
JMLSEMopt.KLR_epsilon = 10^-10;
JMLSEMopt.num_iter = num_iter;
tic
JMLSEM_res = cJMLS_EM_wrap( sys, init, JMLSEMopt);
time(4) = toc;


% tic
% PSEM_res = PSEM_JMLGSS( sys, init, psem_opt );
% time(3) = toc;
%     
% tic
% PSAEM_res = PSAEM_JMLGSS( sys, init, psaem_opt );
% time(2) = toc;

tic
RB_PSAEM_res = RB_PSAEM_JMLGSS( sys, init, rb_psaem_opt );
time(1) = toc;





%%
KLR_Mu = 5;

% Prior
mu0 = sys.z0;
P0 = sys.P0;
lnw0 = log(1);
[~,z,lnw,mu,P] = GM_to_HGM(sys.K,lnw0,mu0,P0);
prior.z = z;
prior.lnw = lnw;
prior.mu = mu;
prior.P = P;

lnL_RB_PSAEM = zeros(num_iter+1,1);
for i = 1:num_iter+1
    param.lnT = RB_PSAEM_res.lnThist(:,:,i);
    param.A=RB_PSAEM_res.Ahist(:,:,:,i);
    param.B=RB_PSAEM_res.Bhist(:,:,:,i);
    param.C=RB_PSAEM_res.Chist(:,:,:,i);
    param.Q=RB_PSAEM_res.Qhist(:,:,:,i);
    param.R=RB_PSAEM_res.Rhist(:,:,:,i);
    tic
    lnL_RB_PSAEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
    toc
end

% 
% lnL_PSAEM = zeros(num_iter+1,1);
% for i = 1:num_iter+1
%     param.lnT = PSAEM_res.lnThist(:,:,i);
%     param.A=PSAEM_res.Ahist(:,:,:,i);
%     param.B=PSAEM_res.Bhist(:,:,:,i);
%     param.C=PSAEM_res.Chist(:,:,:,i);
%     param.Q=PSAEM_res.Qhist(:,:,:,i);
%     param.R=PSAEM_res.Rhist(:,:,:,i);
%     lnL_PSAEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
% end
% 
% 
% lnL_PSEM = zeros(num_iter+1,1);
% for i = 1:num_iter+1
%     param.lnT = PSEM_res.lnThist(:,:,i);
%     param.A=PSEM_res.Ahist(:,:,:,i);
%     param.B=PSEM_res.Bhist(:,:,:,i);
%     param.C=PSEM_res.Chist(:,:,:,i);
%     param.Q=PSEM_res.Qhist(:,:,:,i);
%     param.R=PSEM_res.Rhist(:,:,:,i);
%     lnL_PSEM(i) = Ln_Likelihood_of_soln( sys, param, prior, KLR_Mu);
% end



lnL_JMLSEM = zeros(num_iter+1,1);
for i = 1:num_iter+1
    param.lnT = JMLSEM_res.lnThist(:,:,i);
    param.A=JMLSEM_res.Ahist(:,:,:,i);
    param.B=JMLSEM_res.Bhist(:,:,:,i);
    param.C=JMLSEM_res.Chist(:,:,:,i);
    param.Q=JMLSEM_res.Qhist(:,:,:,i);
    param.R=JMLSEM_res.Rhist(:,:,:,i);
    priori = JMLSEM_res.prior_hist.step(i);
    
    numL = size(priori.cP,3);
    priori.P = nan(sys.nz,sys.nz,numL);
   for l=1:numL
    priori.P(:,:,l) = priori.cP(:,:,l).'*priori.cP(:,:,l);
    end
    
    lnL_JMLSEM(i) = Ln_Likelihood_of_soln( sys, param, priori, KLR_Mu);
end


%%
figure(1);clf;
plot_5(time(1)/num_iter*(0:num_iter).',lnL_RB_PSAEM,2);
hold on;
% plot_5(time(2)/num_iter*(0:num_iter).',lnL_PSAEM,3);
% plot_5(time(3)/num_iter*(0:num_iter).',lnL_PSEM,4);
plot_5(time(4)/num_iter*(0:num_iter).',lnL_JMLSEM,5);

legend('RB-PSAEM','Proposed EM','Location','best');
xlabel('Computational time [s]');
ylabel('ln(p(y))');
title('Log-likelihood vs. Computational time')
% axis([0 min(time) -830 1100])
% set(gca, 'YScale', 'log')
%tight_figure;

%% Bode plots



timestep = 1;%0.001;
dw = 0.01; w = 0.01:dw:pi;


A_arr = cat(4, true_sys.A, init.A, RB_PSAEM_res.Ahist(:,:,:,end),JMLSEM_res.Ahist(:,:,:,end));
B_arr = cat(4, true_sys.B, init.B, RB_PSAEM_res.Bhist(:,:,:,end),JMLSEM_res.Bhist(:,:,:,end));
C_arr = cat(4, true_sys.C, init.C, RB_PSAEM_res.Chist(:,:,:,end),JMLSEM_res.Chist(:,:,:,end));
nM = size(A_arr,4);
[bode_mag,bode_phase] = generate_bodes(w,timestep, A_arr,B_arr,C_arr);

figure(3+i-1);
clf;
a = 0.06;
[ha,~] = tight_subplot(2,1,[a/2 a]*2,[a*2.2 a],[a*2 a/2]);

for i=1:sys.K
    for m = 1:nM
        

        a = bode_mag(:,i,m);
        b = bode_phase(:,i,m);

%         subplot(2,1,1);
        axes(ha(i))
        semilogx_5(w,20*log10((a)),m);
        if (i==1)
            legend('True','Initial Guess','RB-PSAEM','Proposed EM');
        end
        title(['Model ' num2str(i) ' magnitude frequency response']);
        ylabel('Magnitude (dB)');
        hold on
        xlim([w(1) w(end)]);
        set(gca,'fontsize', 18);
        %
% %         subplot(2,1,2);
%         axes(ha(2,i))
%         semilogx_5(w,b,m);
%         ylabel('Phase (deg)');
%         xlabel('Frequency (rad/s)');
%         hold on;
%         xlim([w(1) w(end)]);
%         
%         set(gca,'fontsize', 18);
    if (i~=sys.K)
        set(gca,'xticklabel',{[]})
    end
    end
    
end
xlabel('Frequency (rad/s)');
%tight_figure;


%%



figure(2);clf;
timestep = 1;%0.001;
A = true_sys.A(:,:,1);B = true_sys.B(:,:,1);C = true_sys.C(:,:,1);
SSsys1 = ss(A,B,C,0,timestep);
TF_sys1 = tf(SSsys1)
A = true_sys.A(:,:,2);B = true_sys.B(:,:,2);C = true_sys.C(:,:,2);
SSsys2 = ss(A,B,C,0,timestep);
TF_sys2 = tf(SSsys2)
% A = true_sys.A(:,:,3);B = true_sys.B(:,:,3);C = true_sys.C(:,:,3);
% SSsys3 = ss(A,B,C,0,timestep);

bode(SSsys1,'-b');
hold on;
bode(SSsys2,'-b');
% bode(SSsys3,'-b');

%
A = JMLSEM_res.Ahist(:,:,1,end);B = JMLSEM_res.Bhist(:,:,1,end);C = JMLSEM_res.Chist(:,:,1,end);
SSsys1 = ss(A,B,C,0,timestep);
A = JMLSEM_res.Ahist(:,:,2,end);B = JMLSEM_res.Bhist(:,:,2,end);C = JMLSEM_res.Chist(:,:,2,end);
SSsys2 = ss(A,B,C,0,timestep);
% A = JMLSEM_res.Ahist(:,:,3,end);B = JMLSEM_res.Bhist(:,:,3,end);C = JMLSEM_res.Chist(:,:,3,end);
% SSsys3 = ss(A,B,C,0,timestep);


bode(SSsys1,'--g');
bode(SSsys2,'--g');
% bode(SSsys3,'--g');
% bode(SSsys4,'--r');

% 
% 
A = RB_PSAEM_res.Ahist(:,:,1,end);B = RB_PSAEM_res.Bhist(:,:,1,end);C = RB_PSAEM_res.Chist(:,:,1,end);
SSsys1 = ss(A,B,C,0,timestep);
A = RB_PSAEM_res.Ahist(:,:,2,end);B = RB_PSAEM_res.Bhist(:,:,2,end);C = RB_PSAEM_res.Chist(:,:,2,end);
SSsys2 = ss(A,B,C,0,timestep);
% A = RB_PSAEM_res.Ahist(:,:,3,end);B = RB_PSAEM_res.Bhist(:,:,3,end);C = RB_PSAEM_res.Chist(:,:,3,end);
% SSsys3 = ss(A,B,C,0,timestep);
% A = RB_PSAEM_res.Ahist(:,:,4,end);B = RB_PSAEM_res.Bhist(:,:,4,end);C = RB_PSAEM_res.Chist(:,:,4,end);
% SSsys4 = ss(A,B,C,0);

bode(SSsys1,'--r');
bode(SSsys2,'--r');
% bode(SSsys3,'--r');
% bode(SSsys4,'--g');


% A = PSEM_res.Ahist(:,:,1,end);B = PSEM_res.Bhist(:,:,1,end);C = PSEM_res.Chist(:,:,1,end);
% SSsys1 = ss(A,B,C,0,timestep);
% A = PSEM_res.Ahist(:,:,2,end);B = PSEM_res.Bhist(:,:,2,end);C = PSEM_res.Chist(:,:,2,end);
% SSsys2 = ss(A,B,C,0,timestep);
% A = PSEM_res.Ahist(:,:,3,end);B = PSEM_res.Bhist(:,:,3,end);C = PSEM_res.Chist(:,:,3,end);
% SSsys3 = ss(A,B,C,0,timestep);
% 
% bode(SSsys1,':k');
% bode(SSsys2,':k');
% bode(SSsys3,':k');
% 
% A = PSAEM_res.Ahist(:,:,1,end);B = PSAEM_res.Bhist(:,:,1,end);C = PSAEM_res.Chist(:,:,1,end);
% SSsys1 = ss(A,B,C,0,timestep);
% A = PSAEM_res.Ahist(:,:,2,end);B = PSAEM_res.Bhist(:,:,2,end);C = PSAEM_res.Chist(:,:,2,end);
% SSsys2 = ss(A,B,C,0,timestep);
% A = PSAEM_res.Ahist(:,:,3,end);B = PSAEM_res.Bhist(:,:,3,end);C = PSAEM_res.Chist(:,:,3,end);
% SSsys3 = ss(A,B,C,0,timestep);
% % A = PSAEM_res.Ahist(:,:,4,end);B = PSAEM_res.Bhist(:,:,4,end);C = PSAEM_res.Chist(:,:,4,end);
% % SSsys4 = ss(A,B,C,0);
% 
% bode(SSsys1,':r');
% bode(SSsys2,':r');
% bode(SSsys3,':r');
% bode(SSsys4,':r');
% legend([a,b,c,d,e],'True','Proposed','RB PSAEM','PSEM','PSAEM')

legend('True','','Proposed','','RB PSAEM','')

%%
% % % Manually set the model order, initial conditions seem to have worked well
% % % to make sure it is in the same order as the true model
% % 
% % disp('Proposed Method')
% % print_results(true_sys, JMLSEM_res,[1 2 3])
% % 
% % disp('RB-PSAEM Method')
% % print_results(true_sys, RB_PSAEM_res,[1 2 3])
% % 
% % disp('PSAEM Method')
% % print_results(true_sys, PSAEM_res,[1 2 3])
% % 
% % disp('PSEM Method')
% % print_results(true_sys, PSEM_res,[1 2 3])

%%
% 
% save('results')
% %% PLOTTING
% set(0,'defaulttextinterpreter','LaTex')
% 
% err_bd_prop = zeros(sys.K,num_iter,3);
% 
% for r = 1:4
%     if r == 1; A = RB_PSAEM_res.Ahist; B = RB_PSAEM_res.Bhist; C = RB_PSAEM_res.Chist; Q = RB_PSAEM_res.Qhist; R = RB_PSAEM_res.Rhist; end
%     if r == 2; A = PSAEM_res.Ahist; B = PSAEM_res.Bhist; C = PSAEM_res.Chist; Q = PSAEM_res.Qhist; R = PSAEM_res.Rhist; end
%     if r == 3; A = PSEM_res.Ahist; B = PSEM_res.Bhist; C = PSEM_res.Chist; Q = PSEM_res.Qhist; R = PSEM_res.Rhist; end
%     if r == 4; A = JMLSEM_res.Ahist; B = JMLSEM_res.Bhist; C = JMLSEM_res.Chist; Q = JMLSEM_res.Qhist; R = JMLSEM_res.Rhist; end
%     
%     sysE = cell(sys.K,num_iter);
%     for t = 1:num_iter
%         for k = 1:sys.K
%             sysE{k,t} = ss(A(:,:,k,t),B(:,:,k,t),C(:,:,k,t),0,1);
%         end
%     end
%     
%     mag0 = cell(1,sys.K); magE = cell(1,sys.K);
%     
%     for k = 1:sys.K;
% 
%         % Compute Kalman gain of steady-state true system 
%         [P0,~,K0] = dare(true_sys.A(:,:,k)',true_sys.C(:,:,k)',true_sys.Q(:,:,k), 0);
%         K0 = K0'; % Here K0 = A*K with K = Kalman Gain
% 
%         % True system bode plot
%         dw = 0.01; w = 0.01:dw:pi;
%         [mag0{k}, phase0] = bode(ss(true_sys.A(:,:,k),[true_sys.B(:,:,k) K0],true_sys.C(:,:,k),[zeros(1,sys.nu) 1],1),w);
%         mag0{k} = 20*log10(mag0{k});
%         mag0{k} = reshape(squeeze(mag0{k})',[],1,sys.nu+1);
%         phase0 = reshape(squeeze(phase0)',[],1,sys.nu+1);
%     end
% 
%     for t = num_iter:-1:1
%         for k = 1:sys.K
%             % Q-fix
%             Qfix = 0.5*Q(:,:,k,t) + 0.5*Q(:,:,k,t)';
% 
%             [PE,~,KE] = dare(A(:,:,k,t)',C(:,:,k,t)',Qfix, 0);
%             KE = KE';
%             [magE{k}, phaseE] = bode(ss(A(:,:,k,t),[B(:,:,k,t) KE],C(:,:,k,t),[zeros(1,sys.nu) 1],1),w);
%             magE{k} = 20*log10(magE{k});
%             magE{k} = reshape(squeeze(magE{k})',[],1,sys.nu+1);
%             phaseE = reshape(squeeze(phaseE)',[],1,sys.nu+1);
%             if t < num_iter
%                 err_bd_prop(k,t,r) = sqrt(sum(sum(abs(10.^(mag0{k}/20) - 10.^(magE{P(k)}/20)).^2.*dw))/pi);
%             end
%         end
%         if t == num_iter % Pair modes
%             err = zeros(1,sys.K);
%             for k = 1:sys.K
%                 for l = 1:sys.K
%                     err(l,k) = sqrt(sum(sum(abs(10.^(mag0{l}/20) - 10.^(magE{k}/20)).^2*dw))/pi);
%                 end
%             end
%             for k = 1:sys.K
%                 [~,temp] = min(err(:)); [itemp,jtemp] = ind2sub(size(err),temp); P(itemp) = jtemp; err(:,jtemp) = Inf; err(itemp,:) = Inf;
%             end
%             
%             for k = 1:sys.K
%                 err_bd_prop(k,t,r) = sqrt(sum(sum(abs(10.^(mag0{k}/20) - 10.^(magE{P(k)}/20)).^2.*dw))/pi);
%             end
%         end
%     end
% end
% 
% figure(1); clf
% colors = 1/256.*[0,0,0; 0,0,255; 150,150,150; 255,0,0]; style{1} = '-'; style{2} = '-'; style{3} = '-'; style{4} = '-';
% err_bd_prop_mean = squeeze(mean(mean(err_bd_prop),1));
% for r = 1:4
%     loglog(time(r)/num_iter*(1:num_iter),err_bd_prop_mean(1:num_iter,r),style{r},'Color',colors(r,:),'LineWidth',2.5)
%     hold on
% end
% 
% set(gca,'FontSize',12)
% hLegend = legend(['RB-PSAEM, N = ',num2str(N1)],['PSAEM, N = ',num2str(N2)],['PSEM, N = ',num2str(N3)]);
% set(hLegend,'FontSize',20,'Interpreter','LaTex','location','best','box','off');
% hold off
% xlabel('Computation time [s]','FontSize',20)
% ylabel('Mean $\mathcal{H}_2$ error','FontSize',20)
% 
% 
% if exist('RB_PSAEM_res','var') && sys.nu == 1 && sys.ny == 1 % Only for SISO systems
%     figure(2); clf
% 
%     Ht = zeros(sys.K,100); He = zeros(sys.K,100); Hi = zeros(sys.K,100);
%     SYS=JMLSEM_res;
% %     SYS=RB_PSAEM_res;
%     for k = 1:sys.K
%         stk = minreal(ss(true_sys.A(:,:,k),true_sys.B(:,:,k),true_sys.C(:,:,k),zeros(sys.ny,sys.nu),1));
%         wout = linspace(0.1,pi); [Ht(k,:),~] = freqresp(stk,wout);
%         sek = ss(SYS.Ahist(:,:,k,end),SYS.Bhist(:,:,k,end),SYS.Chist(:,:,k,end),zeros(sys.ny,sys.nu),1);
%         sik = ss(init.A(:,:,k),init.B(:,:,k),init.C(:,:,k),zeros(sys.ny,sys.nu),1);
%         [He(k,:),~] = freqresp(sek,wout);
%         [Hi(k,:),~] = freqresp(sik,wout);
%     end
% 
% 
%     for k = 1:sys.K
%         H_i = loglog(wout,abs(squeeze(Hi(k,:))),':','Color',[1 0.3 0.3],'LineWidth',1.5);
%         hold on
%         H_t = loglog(wout,abs(squeeze(Ht(k,:))),'--','Color',0.6*[1 1 1],'LineWidth',5);
%         H_e = loglog(wout,abs(squeeze(He(k,:))),'k','LineWidth',1.5);
%     end
%     set(gca,'Position',[0.2 0.2 0.7 0.7])
%     xlim([min(wout) max(wout)])
%     xlabel('Frequency $\omega$','FontSize',20)
%     ylabel('Gain','FontSize',20)
%     hLegend = legend([H_t H_e H_i],'True','Estimated (RB-PSAEM)','Intial guess');
%     set(hLegend,'FontSize',20,'Interpreter','LaTex','location','best','box','off');
%     hold off
%     set(gca,'FontSize',12)
% end
