function [ewjmwi,Bij_reli,Lij,sij,rij] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon)


[Uxi,Di] = svd(Li);
 ni = sum(Di(:) > epsilon);
 Ui = Uxi(:,1:ni);
 Zi = Uxi(:,ni+1:end);
 
[Uxj,Dj] = svd(Lj);
nj = sum(Dj(:) > epsilon);
Uj = Uxj(:,1:nj);
Zj = Uxj(:,nj+1:end);

% check for compatible range spaces
if (ni ~= nj) % range spaces can't possibly be equal with different rank
    Bij_reli = inf;
    return;
end

% Check null and range space basis have no overlap (dot product zero)
Ck1 = Ui.'*Zj;
Ck2 = Uj.'*Zi;

if any(abs(Ck1(:)) > epsilon) || any(abs(Ck2(:)) > epsilon)
    Bij_reli = inf;
    return;
end

Sigmai = Di(1:ni,1:ni);


Sigmaj = Ui.'*Lj(1:ni,1:ni)*Ui; % we already checked ni=nj

etai = Ui.'*si;
etaj = Ui.'*sj;

invSigmai = eye(ni) / Sigmai;
invSigmaj = eye(ni) / Sigmaj;

%ri and rj stuff
twoPi_ni = ni*log(2*pi);
logdetSigmai = logdet(Sigmai);
logdetSigmaj = logdet(Sigmaj);
ei = -0.5*(ri-etai.'/Sigmai*etai + logdetSigmai);
ej = -0.5*(rj-etaj.'/Sigmaj*etaj + logdetSigmaj);
ewjmwi = exp(ej-ei);
if nargout > 1
    ai = exp(ei + 0.5*twoPi_ni);
    aj = exp(ej + 0.5*twoPi_ni);

    vi = ai/(ai+aj);
    vj = aj/(ai+aj);
    mui = Sigmai\etai;
    muj = Sigmaj\etaj;
    invSigmaij = vi*invSigmai+vj*invSigmaj+vi*vj*(mui-muj)*(mui-muj).';
    logdetinvSigmaij = logdet(invSigmaij);
    Bij_reli = (ai+aj)*logdetinvSigmaij+ai*logdetSigmai+aj*logdetSigmaj;

    if nargout > 2
        etaij = invSigmaij\(vi*mui+vj*muj);
        rij = etaij.'*invSigmaij*etaij - 2*log(ai+aj) + twoPi_ni + logdetinvSigmaij;
        sij = Ui*etaij;
        Lij = Ui/invSigmaij*Ui.';
    end
end
