function C = chol_SPD(A)


[L,D] = ldl(A);

C = (D.^0.5) * L.';