function wout = LSE(win)
% vectorwise LSE trick along the first dimension of a matrix (each column). 
wstar = max(win);

% protection against columns being entirely -inf
idx = (wstar == -inf);

wout = wstar + log(sum(exp(win-wstar)));

wout(idx) = -inf;